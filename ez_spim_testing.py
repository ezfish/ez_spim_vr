from pymba import *
import time
import numpy as np
import cv2
from ezspim.lib.semmelhack_2014.tailfit import TailFit
np.set_printoptions(precision=2)

running = True;
processing = False;
def handleclick(event,x,y,flags,param):
    global running
    if event==cv2.EVENT_LBUTTONDOWN:
        print("QUITTING")
        running = False;

        
tf = TailFit(handleclick);
def onFrCaptrue(framecapture):
    global running
    global processing
    global drawlock
    global drawdata
    starttime = time.perf_counter()
    if not running:
        return
    
    if not processing:
        processing = False;#TODO: might want True here
        frame = np.ndarray(buffer = framecapture.getBufferByteData(),
                            dtype = np.uint8,
                            shape = (framecapture.height,
                                    framecapture.width,
                                    1))
        frame = cv2.resize(frame, (int(framecapture.width/2), int(framecapture.height/2)))
        frame = np.swapaxes(frame,0,1)
        #framecapture.queueFrameCapture(onFrCaptrue);
        framecapture.queueFrameCapture();
        tf.tailfit(frame)
        processing = False;
    else:
        #framecapture.queueFrameCapture(onFrCaptrue);
        framecapture.queueFrameCapture();
    endtime = time.perf_counter()
    return framecapture
    #print(str((endtime-starttime)*1000))

    

with Vimba() as vimba:
    # get system object
    system = vimba.getSystem()
    
    cameraIds = vimba.getCameraIds()
    for cameraId in cameraIds:
        print(cameraId)
    
    # get and open a camera
    camera0 = vimba.getCamera(cameraIds[0])
    camera0.openCamera()
    
    cameraFeatureNames = camera0.getFeatureNames()
    for name in cameraFeatureNames:
        print(name)

    # get the value of a feature
    print(camera0.AcquisitionMode)
    
    framecapture = camera0.getFrame()    # creates a frame
    framecapture.announceFrame()
    camera0.startCapture()
    #framecapture.queueFrameCapture(onFrCaptrue);

    ################
    #LOOP START
    framecapture.queueFrameCapture();
    camera0.runFeatureCommand('AcquisitionStart')
    
    while running:
        framecapture.waitFrameCapture(1000)
        framecapture = onFrCaptrue(framecapture)

    #LOOP END
    ################


    #camera0.runFeatureCommand('AcquisitionStart')
    #while running:
    #    time.sleep(1)
    print("END CAPTURE")
    camera0.endCapture()
    print("REVOKE FRAMES")
    camera0.revokeAllFrames()
    print("AQ STOP")
    camera0.runFeatureCommand('AcquisitionStop')
    print("DONE")