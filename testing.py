if __name__ == '__main__':
    from psychopy import visual
    from psychopy import event #import some libraries from PsychoPy
    import random
    from datetime import datetime, timedelta
    from ezspim.core.ClosedLoopWiggleModel import ClosedLoopWiggleModel
    from ezspim.core.SimpleClosedLoopWiggleModel import SimpleClosedLoopWiggleModel
    from ezspim.core.AreaClosedLoopWiggleModel import AreaClosedLoopWiggleModel
    from ezspim.core.GratingStimulator import GratingStimulator
    from ezspim.core.CameraHandler import CameraHandler
    from ezspim.core.DataLogger import DataLogger
    from ezspim.core.DaqManager import DaqManager
    from ezspim.core.regime.OpenLoopRegime import OpenLoopRegime
    from ezspim.experiments.GainNestedExperiment import GainNestedExperiment
    import numpy as np
    from psychopy import core
    from random import shuffle
    import psutil
    import atexit
    import os
    from datetime import datetime

def handleclick(event,x,y,flags,param):
    return
    global done
    if event==cv2.EVENT_LBUTTONDOWN:
        print("QUITTING")
        done = True;
        CameraHandler.halt()

if __name__ == '__main__':
    p = psutil.Process()
    p.cpu_affinity([7])
    p.nice(psutil.REALTIME_PRIORITY_CLASS)
    
    wait_for_clock_signal =  False

    gs = GratingStimulator([1280,720], True)
    gs.draw(0.0);

    wm = AreaClosedLoopWiggleModel(tau_accum=0.025, tau_decay=0.01, normalizer =21000) #~4000 - 20000 according to Daniel's free swimimng model.
    #wm = AreaClosedLoopWiggleModel(tau_accum=0.008, tau_decay=0.008, normalizer =100000) #~4000 - 20000 according to Daniel's free swimimng model.
    #wm = SimpleClosedLoopWiggleModel(tau_accum=0.025, tau_decay=0.001, normalizer =10000) #~4000 - 20000 according to Daniel's free swimimng model.
    #wm.enable_plotting()
    daq = DaqManager()

    #velocities = np.asarray([3, 5, 8, 10, 15, 25, 30, 40],np.float32)/5 #mm, values from Severi 2014 downscaled by 5 for virtual screen 1mm away
    #velocities = np.asarray([10, 10],np.float32)/5
    #velocities = random.sample(velocities, len(velocities))
    #gains = [0.1, 0.1, 0.1, 0.1, 0.1, 0.25, 0.25, 0.25, 0.25, 0.25, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 1, 1, 1, 1, 1.5, 1.5, 1.5, 1.5, 1.5]
    
    #experiment_name = "Landscape_around_Speed_10_Gain_1_2"
    experiment_name = "test"

    velocities = np.asarray([2, 5, 10, 15, 20, 2, 5, 10, 15, 20, 2, 5, 10, 15, 20, 2, 5, 10, 15, 20, 2, 5, 10, 15, 20],np.float32)/5
    gains = [0.3, 0.3, 0.3, 0.6, 0.6, 0.6, 1.0, 1.0, 1.0, 1.3, 1.3, 1.3, 1.6, 1.6, 1.6]+[0.3, 0.3, 0.3, 0.6, 0.6, 0.6, 1.0, 1.0, 1.0, 1.3, 1.3, 1.3, 1.6, 1.6, 1.6]+[0.3, 0.3, 0.3, 0.6, 0.6, 0.6, 1.0, 1.0, 1.0, 1.3, 1.3, 1.3, 1.6, 1.6, 1.6]

    
    #velocities = np.asarray([2, 2, 2, 4, 4, 4, 6, 6, 6, 8, 8, 8, 10, 10, 10, 12, 12, 12, 14, 14, 14],np.float32)/5
    #velocities = np.asarray([2, 4, 6, 8, 10, 12, 14, 2, 4, 6, 8, 10, 12, 14, 2, 4, 6, 8, 10, 12, 14],np.float32)/5
    #velocities = np.asarray([5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5],np.float32)/5
    #gains = [0.5,  1.5]


    #gains = [0.8, 0.8, 0.8, 1.0, 1.0, 1.0, 1.2, 1.2, 1.2, 0.8, 0.8, 0.8, 1.0, 1.0, 1.0, 1.2, 1.2, 1.2, ]             
    #velocities = np.asarray([2.5, 2.5, 2.5, 5, 5, 5, 7.5, 7.5, 7.5, 2.5, 2.5, 2.5, 5, 5, 5, 7.5, 7.5, 7.5, 2.5, 2.5, 2.5, 5, 5, 5, 7.5, 7.5, 7.5],np.float32)/5
    
    #velocities = np.asarray([10],np.float32)/5
    #gains = [1, 1, 1, 1, 1]
    #gains = [0.6, 0.6, 0.8, 0.8, 1.0, 1.0, 1.2, 1.2, 1.4, 1.4]
    #gains = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0]

    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    dir = 'H:\\experiments\\'+dt+"_"+experiment_name+"\\"
    print("Saving to: {}".format(dir))
    os.mkdir(dir)
    experiment = GainNestedExperiment(dir,gs,daq,velocities,gains,wm, dur_stand =10, dur_closed = 20)

    CameraHandler.init(fit_tail=True,tf_callback=handleclick,showcam=False);
    if wait_for_clock_signal: daq.block()
    CameraHandler.start()

    @atexit.register
    def halt():
        print("HALTING")
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()

    try:
        experiment.run()
    finally:
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()
    print("DONE")
        #if __name__ == '__main__':
        #    core.quit()
