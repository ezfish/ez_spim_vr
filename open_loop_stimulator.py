from psychopy import visual
from psychopy import event #import some libraries from PsychoPy
import random
from datetime import datetime, timedelta
from ezspim.core.OpenLoopWiggleModel import OpenLoopWiggleModel
from ezspim.core.GratingStimulator import GratingStimulator
from ezspim.core.CameraHandler import CameraHandler
from ezspim.core.DataLogger import DataLogger
from ezspim.core.DaqManager import DaqManager
from ezspim.core.regime.OpenLoopRegime import OpenLoopRegime
from ezspim.experiments.SimpleOpenLoopExperiment import SimpleOpenLoopExperiment
import numpy as np
if __name__ == '__main__':
    from psychopy import core
from random import shuffle

if __name__ == '__main__':
    wait_for_clock_signal = True

    def handleclick(event,x,y,flags,param):
        global done
        if event==cv2.EVENT_LBUTTONDOWN:
            print("QUITTING")
            done = True;
            CameraHandler.halt()

    mywin = visual.Window([1280,720],units="cm", color=(0, 0, 0), colorSpace='rgb',fullscr=True, screen=1, monitor="testMonitor")


    wm = OpenLoopWiggleModel()
    gs = GratingStimulator(mywin)
    daq = DaqManager()

    #velocities = np.asarray([3, 5, 8, 10, 15, 25, 30, 40],np.float32)/5 #mm, values from Severi 2014 downscaled by 5 for virtual screen 1mm away
    velocities = np.asarray([25],np.float32)/5 #mm, values from Severi 2014 downscaled by 5 for virtual screen 1mm away
    velocities = random.sample(velocities, len(velocities))
    experiment = SimpleOpenLoopExperiment('H:\\experiments\\',gs,daq,velocities,wm, dur_stand = 30, dur_grat = 30, repeats=30)

    CameraHandler.init(fit_tail=False);
    if wait_for_clock_signal: daq.block()
    CameraHandler.start()

    try:
        experiment.run()
    finally:
        CameraHandler.halt()
        #mywin.close()
        daq.close()

    print("DONE")
    #if __name__ == '__main__':
    #    core.quit()
