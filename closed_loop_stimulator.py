
# -*- coding: utf-8 -*-
"""
Simpler_gratings_VR 0.2, a fork of Simpler gratings

Create a simple grating stimulus and alternate to a neutral background based on
the timing signal from the camera.
@author: Joseph Chan and Joe Baines-Holmes
"""
from psychopy import visual
from psychopy import event #import some libraries from PsychoPy
if __name__ == '__main__':
    from psychopy import core
from pyDAQmxReadCounter import frameCounter
from pyDAQmxReadAI import ReadDAQmxAI
import json
import random
from datetime import datetime, timedelta
from ClosedLoopWiggleModel import ClosedLoopWiggleModel
import numpy as np
from random import shuffle
from pymba import *
import time
import cv2
import atexit
import time
from semmelhack_2014.tailfit import TailFit


experimentName = 'H:\\experiments\\experiment-' + datetime.now().isoformat().split('.')[0].replace(':','') + '.json'
frame_id = 0
done = False
processing = False;
#video_out = cv2.VideoWriter(experimentName+'.avi', -1, 100.0, (320,256))


def handleclick(event,x,y,flags,param):
    global done
    if event==cv2.EVENT_LBUTTONDOWN:
        print("QUITTING")
        done = True;

tf = TailFit(handleclick);
wm = ClosedLoopWiggleModel()

def onFrCaptrue(framecapture):
    global frame_id
    global done
    global video_out
    global processing
    global wm
    #starttime = time.time()
    if done:
        return

    if not processing:
        processing = False;#TODO: might want True here
        frame = np.ndarray(buffer = framecapture.getBufferByteData(),
                            dtype = np.uint8,
                            shape = (framecapture.height,
                                    framecapture.width,
                                    1))
        frame = np.swapaxes(frame,0,1)
        framecapture.queueFrameCapture(onFrCaptrue);
        tailpoints = tf.tailfit(frame)
        wm.processTailpoints(tailpoints)
        frame_id+=1
        processing = False;
    else:
        framecapture.queueFrameCapture(onFrCaptrue);
    endtime = time.perf_counter()

    #frame = np.ndarray(buffer = framecapture.getBufferByteData(),
    #                    dtype = np.uint8,
    #                    shape = (framecapture.height,
    #                            framecapture.width,
    #                            1))
    #frame = cv2.resize(frame, (320, 256)) 
    #video_out.write(frame)
    #cv2.imshow("Tail",frame)
    #cv2.waitKey(1)
    #frame_id+=1
    #print(frame_id)
    #framecapture.queueFrameCapture(onFrCaptrue);
    #endtime = time.time()
    #print(endtime-starttime)

    
np.set_printoptions(threshold=np.inf)
aiReader = ReadDAQmxAI(samples_to_read=10000, volt_range=[-0.05, 3.5],analogue_channel='AI1');
mywin = visual.Window([1280,720],units="cm", color=(0, 0, 0), colorSpace='rgb',fullscr=True, screen=0, monitor="testMonitor")
counter = frameCounter()

mm1 = 0.8
# Variables ----------------
stim_length = 20 #in seconds
rest_length = 10 #in seconds
spatialfreq = mm1/2
orientation = 270 #in degrees
wait_for_clock_signal = False
# --------------------------
# Create protocol
#velocities = [0,1,2,3,4,5,6]
velocities = np.asarray([3, 5, 8, 10, 15, 25, 30, 40],np.float32)/5*2/10 #cm
velocities = random.sample(velocities, len(velocities))
grating_counter = 0;
def onGratingStart():
    global grating_counter
    wm.setVelocity(0.4)
    #wm.setVelocity(velocities[grating_counter%len(velocities)])
    grating_counter += 1 
def onGratingEnd():
    pass
    #wm.setVelocity(0)     

#####################
#Prepare camera
vimba = Vimba()
vimba.startup()
# get system object
system = vimba.getSystem()
cameraIds = vimba.getCameraIds()
for cameraId in cameraIds:
    print(cameraId)
# get and open a camera
camera0 = vimba.getCamera(cameraIds[0])
camera0.openCamera()
# get the value of a feature
print(camera0.AcquisitionMode)


# --------------------------
#create grating
grating = visual.GratingStim(win=mywin, tex='sqr',mask= 'none',units='cm', pos=(0,0),size= (600,800),sf=spatialfreq)
fixation = visual.ImageStim(win = mywin, image=None, mask=None, units='', pos=(0.0, 0.0), size=None, ori=0.0, color=(0, 0, 0), colorSpace='rgb')

grating.setOri(orientation)


if wait_for_clock_signal:
    while counter.waitForFrameCounter()[0] == 0:
        print("Waiting for signal")
        continue
time_experiment_start = time.perf_counter()
timestamp = time.perf_counter()

#Start tail camera
framecapture = camera0.getFrame()    # creates a frame
framecapture.announceFrame()
camera0.startCapture()
#framecapture.queueFrameCapture(onFrCaptrue);
camera0.runFeatureCommand('AcquisitionStart')


experimentInfo = []
timestampInfo = []
isGrating = False
frameCount = 0
experimentInfo.append([timestamp, frameCount, wm.position, float(wm.velocity), wm.acceleration, isGrating, frame_id])
switch_stamp = time.perf_counter() - time_experiment_start
print("Stimulus started {}, {}".format(switch_stamp, isGrating))
try:
    while True:
        #print(frame_id)
        oldts = timestamp
        timestamp = time.perf_counter() - time_experiment_start
        tdelta = timestamp - oldts
        wm.loop(tdelta)
        #Switch grating on and off
        if isGrating:
            if timestamp - switch_stamp > stim_length:
                isGrating = False
                onGratingEnd()
                switch_stamp = time.perf_counter() - time_experiment_start
        if not isGrating:
            if timestamp - switch_stamp > rest_length:
                isGrating = True
                onGratingStart()
                switch_stamp = time.perf_counter() - time_experiment_start
        #Write timestamp info
        experimentInfo.append([timestamp, frameCount, float(wm.position), float(wm.velocity), float(wm.acceleration), isGrating, frame_id])
        #Write frame info
        frameCountPrev = frameCount
        frameCount = getFrameCount()[0]
        if frameCount is not frameCountPrev:
            experimentInfo.append([timestamp, frameCount, float(wm.position), float(wm.velocity), float(wm.acceleration), isGrating, frame_id])
            windowStart = getFrameCount()[0]
        if isGrating:
            grating.phase = wm.position*2.5
            grating.draw()
            mywin.flip()
        else:
            grating.phase = wm.position*2.5
            grating.draw()
            mywin.flip()
            '''
            fixation.draw()
            mywin.flip()
            '''
            
        if len(event.getKeys())>0: 
            break
        
        event.clearEvents()

    counter.close()
 
#cleanup
finally:
    #mywin.saveMovieFrames('stim.tif')
    mywin.close()
    done = True
    experimentInfo = [dict(zip(["TimeStamp", "FrameCount", "Position", "Velocity", "Acceleration", "IsGrating", "TailCamFrameId"], a)) for a in experimentInfo]
    for entry in experimentInfo:
        entry['FrameCount'] = int(entry['FrameCount'])
    experimentInfo = {'data':experimentInfo, 
                      'date': datetime.now().isoformat()}
    infoFile = open(experimentName, "w")
    #print json.dumps(experimentInfo, indent=2)
    infoFile.write(json.dumps(experimentInfo, indent=2))
    infoFile.close()
    video_out.release()
    
    print("END CAPTURE")
    camera0.endCapture()
    print("REVOKE FRAMES")
    camera0.revokeAllFrames()
    print("AQ STOP")
    camera0.runFeatureCommand('AcquisitionStop')
print("DONE")
    #if __name__ == '__main__':
    #    core.quit()
