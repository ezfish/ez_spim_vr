
if __name__ == '__main__':
    import pyximport; pyximport.install()
    from psychopy import visual
    from psychopy import event #import some libraries from PsychoPy
    import random
    from datetime import datetime, timedelta
    import numpy as np
    from psychopy import core
    from random import shuffle
    import psutil
    import atexit
    import os
    import time
    from datetime import datetime
    import signal

    from ezspim.core.GratingStimulator import GratingStimulator
    from ezspim.core.CameraHandler import CameraHandler
    from ezspim.core.DataLogger import DataLogger
    from ezspim.core.DaqManager import DaqManager
    from ezspim.core.Battery import Battery
    from ezspim.core.SampleDetails import SampleDetails

    from ezspim.experiments.IntermittentExperiment import IntermittentExperiment
    from ezspim.experiments.AdjustmentExperiment import AdjustmentExperiment
    from ezspim.core.wm.AreaClosedLoopWiggleModel import AreaClosedLoopWiggleModel

def handleclick(event,x,y,flags,param):
    return

if __name__ == '__main__':
    p = psutil.Process()
    p.cpu_affinity([6,7])
    p.nice(psutil.REALTIME_PRIORITY_CLASS)
    
    wait_for_clock_signal =  False

    gs = GratingStimulator([1280,720], True)
    daq = DaqManager()
    wmC = AreaClosedLoopWiggleModel(tau_accum=0.025, tau_decay=0.01, normalizer =21000, track_bouts=True, threshold=0.1) #~4000 - 20000 according to Daniel's free swimimng model.
    
    #########################################################
    # SETTINGS
    global_tags = ["Test"]
   
    #Sample Details
    Sample_ID = 147 #Sample ID.
    Age = 6 #Age if fish in dpf.
    Genotype = ["HuC:H2B-GCaMP6f","Nacre","1307ix"]
    Description = "mismatch hunt, tectum"
    sd = SampleDetails(Sample_ID, Age, Genotype, Description)

    #########################################################

    #Prepare directories
    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    base_dir = 'C:\\experiments\\'
    DataLogger.base_dir = base_dir;
    print("Saving to: {}".format(base_dir))

    #Battery
    battery_dir = dt+'_battery\\'
    os.mkdir(base_dir+battery_dir)
    battery = Battery(battery_dir)
    
    velocities = np.asarray([5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],np.float32)/5
    gains = [1]
   

    #experiment - adjustment
    experiment_name = "adjustmentexperiment_0"
    description = "find normaliser to set gain = 1"
    tags = global_tags + []
    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    experiment_dir = dt+"_"+experiment_name+"\\"
    os.mkdir(base_dir+battery_dir+experiment_dir)
    experiment = AdjustmentExperiment(battery_dir+experiment_dir,gs,daq,sd,5.0/5.0,1.0,wmC,name=experiment_name, description=description, tags=tags, dur_stand =10, dur_closed = 10, stability_requirement=0.05) #stability used to be 0.05
    battery.append(experiment)
    #experiment.run()

    #Experiment 
    #experiment_name = "Intermittent_Experiment"
    #description = "find mismatch cells"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + [""]
    #experiment = IntermittentExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains, wmC,name=experiment_name, description=description, tags=tags, dur_intermittent = 20, dur_closed =20, likelihood = 0.5)
    #battery.append(experiment)

    #########################################################

    CameraHandler.init(fit_tail=True,showcam=False);
    time.sleep(3)
    CameraHandler.start()
    time.sleep(3)

    
    def halt():
        print("HALTING")
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()
    atexit.register(halt)
    signal.signal(signal.SIGTERM, halt)
    signal.signal(signal.SIGINT, halt)
    #signal.signal(signal.CTRL_BREAK_EVENT, halt)

    # run adjustment Experimebnt by itself...



    #if wait_for_clock_signal: daq.block()
    
    try:
        battery.run()
        print("DONEDONEDONEDONEDONEDONEDONEDONEDONEDONE")

    finally:
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()
    print("DONE")
        #if __name__ == '__main__':
        #    core.quit()
