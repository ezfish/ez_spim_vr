# -*- coding: utf-8 -*-
"""
pyDAQmxReadCounter 0.3.1

An object to read a digital counter from an NI Board.
@author: Joe Baines-Holmes
"""
from ctypes import *
import numpy as np
from PyDAQmx import *

class frameCounter:
    #from PyDAQmx import *
    def __init__(self):
        self.counterTaskHandle = TaskHandle(0)
        self.data = np.zeros((1,), dtype=np.uint32)
        try:
            DAQmxCreateTask("counterTask", byref(self.counterTaskHandle))
            DAQmxCreateCICountEdgesChan(self.counterTaskHandle, "/Dev1/Ctr0", "counterVChan", DAQmx_Val_Rising, 0, DAQmx_Val_CountUp)
            DAQmxSetCICountEdgesTerm(self.counterTaskHandle, "/Dev1/Ctr0","/Dev1/PFI0")    
            DAQmxStartTask(self.counterTaskHandle)
        except Exception as e:
            print(e)
            self.clear()

#    def readCounter(self,counterTaskHandle=self.counterTaskHandle,numpyArrayUint32=self.data,size=1):
    def readCounter(self,size=1,timeOut=DAQmx_Val_WaitInfinitely):
        numpyArrayUint32=self.data
        # Make this wait somehow...
        DAQmxReadCounterU32(self.counterTaskHandle, DAQmx_Val_Auto, timeOut,
                            numpyArrayUint32, size, None, None)
        return self.data
    
    def waitForFrameCounter(self): 
        return self.readCounter(timeOut=DAQmx_Val_WaitInfinitely)
                       
    def close(self):
        DAQmxStopTask(self.counterTaskHandle)
        DAQmxClearTask(self.counterTaskHandle)
    
    def config(self):
        DAQmxStartTask(self.counterTaskHandle)
    
    def clear(self):
        if self.counterTaskHandle:
            print("Clearing task")
            DAQmxStopTask(self.counterTaskHandle)
            DAQmxClearTask(self.counterTaskHandle)

