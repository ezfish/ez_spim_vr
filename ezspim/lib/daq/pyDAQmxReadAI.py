# -*- coding: utf-8 -*-
"""
pyDAQmxReadCIandAI 0.2, adapted from pyDAQmxReadCounter 0.3.1

An object to read a digital counter and analogue input from an NI Board.
@author: Joe Baines-Holmes
"""

from ctypes import *
import numpy as np
from PyDAQmx import *


class ReadDAQmxAI:

    def __init__(self, board='/Dev1/',
                 analogue_channel='AI0', volt_range=[-0.05, 0.05], 
                 samples_to_read=300, counter_tasks=2):
        self.AITaskHandle = TaskHandle(0)
        self.samples_to_read = samples_to_read
        try:
            # Create task for the Analogue Input.
            DAQmxCreateTask("AITask", byref(self.AITaskHandle))
            # Create the analogue input channel (1 channel at the moment).
            DAQmxCreateAIVoltageChan(self.AITaskHandle, board+analogue_channel, "AIChan",
                                     DAQmx_Val_Diff, volt_range[0], volt_range[1], DAQmx_Val_Volts, None)
            # Configure the sample rate.
            DAQmxCfgSampClkTiming(self.AITaskHandle,
                                  None,  # Source channel of the sample clock (set to internal).
                                  10000,  # Sample rate [s/S/Ch.].
                                  DAQmx_Val_Rising,  # Generate samples on the leading edge.
                                  DAQmx_Val_ContSamps,  # Acquire samples continuously until the task is stopped.
                                  self.samples_to_read)  # Size of the buffer (in continuous mode).
            # Commit the task to the board if possible.
            DAQmxTaskControl(self.AITaskHandle, DAQmx_Val_Task_Commit)
        except Exception as e:
            print(e)
            self.clear()
            
    def readInputSamples(self):
        """
        Assumes that this function is being called much more frequently than the time it takes to read the samples.
        """
        ai_data = np.zeros((self.samples_to_read,), dtype=np.float64)
        ai_data_size = ai_data.size
        samples_read = int32()
        try:
            DAQmxReadAnalogF64(self.AITaskHandle,  # Task handle.
                               self.samples_to_read,  # Number of samples per channel. Set to auto to make the code wait for the
                                                #  samples, this shouldn't be a problem if precise transitions are not
                                                #  needed as they are in the OMR experiment.
                               0,  # Non-blocking
                               DAQmx_Val_GroupByChannel,  # How the samples are grouped. In this case each channel occupies
                                                          #  a contiguous portion of the readout array.
                               ai_data,  # Readout array.
                               ai_data_size,  # Size of the readout array in samples.
                               byref(samples_read),  # Samples read into the array (useful if the read mode is continuous).
                               None)
        except Exception as e:    
            return ai_data, samples_read.value
        return ai_data, samples_read.value

    def close(self):
        DAQmxStopTask(self.AITaskHandle)
        DAQmxClearTask(self.AITaskHandle)

    def config(self):
        DAQmxStartTask(self.AITaskHandle)

    def clear(self):
        return;
            #DAQmxStopTask(self.AITaskHandle)
            #DAQmxClearTask(self.AITaskHandle)

