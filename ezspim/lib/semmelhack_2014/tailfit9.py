__version__ = '0.9.3'

import cv2
import time
import numpy as np
import pylab, os
import pdb
import shelve
import sys
from PIL import Image
from filepicker import *
import scipy.ndimage
import scipy.stats
##from sklearn.mixture import GMM
##import cProfile
import hashlib
from tailfitresult import *
def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.digest()

def normalizetailfit(tailfit):
    """Takes in a tailfit, and returns a normalized version which is goes from 0 to 1 normalized to taillength averaged over the first few frames
    """
    tail_length = (tailfit[0][-1,:]-tailfit[0][0,:]).max()
    return [(frame-frame[0,:]).astype(float)/tail_length for frame in tailfit]
    #could angular adjust, and maybe take average of some non-moving frames

def sliding_average(somelist, window_size = 10):
    somelistpadded = np.lib.pad(somelist,(window_size/2,window_size/2),'edge')
    return np.convolve(somelistpadded, np.ones(int(window_size))/float(window_size),mode='valid')

def sliding_gauss(somelist, window_size = 10,sigma=3):
    somelistpadded = np.lib.pad(somelist,(window_size/2,window_size/2),'edge')
    normpdf = scipy.stats.norm.pdf(range(-int(window_size/2),int(window_size/2)),0,sigma)
    return np.convolve(somelistpadded,  normpdf/np.sum(normpdf),mode='valid')[:len(somelist)]

def handleclick(event,x,y,flags,param):
    if event==cv2.EVENT_LBUTTONDOWN:    
        param[0]=x
        param[1]=y


def tail_func2(x, mu, sigma, scale, offset):
    return scale * np.exp(-(x-mu)**4/(2.0*sigma**2))**.2 + offset #
##################################################################
def zeroone(thing):
    return (thing-thing.min())/(np.percentile(thing,99)-thing.min())
def scalesize(frame, multiple):
    return cv2.resize(frame,(frame.shape[0]*multiple,frame.shape[1]*multiple))
#######

def tailfit(filename,display=None,start_point=None, direction='right', output_jpegs = False, plotlengths = False):
    '''
    Takes an avi filepath, fits the tail of the fish
    Display sets if the fit is shown as it is processed (slower)
    Start point is where fitting begins, if None the user is queried
    Direction is which direction the fit happens
    '''
    directions={"up":[0,-1],"down":[0,1],"left":[-1,0],"right":[1,0]}
    fitted_tail=[]

##    print filename, os.path.exists(filename)
    # get system object
    vimba=Vimba()
    vimba.startup()
    system = vimba.getSystem()
    
    # list available cameras (after enabling discovery for GigE cameras)
    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(0.2)
    cameraIds = vimba.getCameraIds()
    
    # get and open a camera
    camera0 = vimba.getCamera(cameraIds[0])
    camera0.openCamera()
    
    # list camera features
    cameraFeatureNames = camera0.getFeatureNames()
    for name in cameraFeatureNames:
        print(name)
    
    # get the value of a feature
    print(camera0.AcquisitionMode)
    
    # set the value of a feature
    camera0.AcquisitionMode = 'SingleFrame'
    
    # create new frames for the camera
    frame0 = camera0.getFrame()    # creates a frame
    
    # announce frame
    frame0.announceFrame()
    
    # capture a camera image
    camera0.startCapture()
    frame0.queueFrameCapture()
    camera0.runFeatureCommand('AcquisitionStart')
    camera0.runFeatureCommand('AcquisitionStop')
    frame0.waitFrameCapture()
    
    # get image data...
    imgData = frame0.getBufferByteData()
    
    # ...or use NumPy for fast image display (for use with OpenCV, etc)
    import numpy as np
    frame = np.ndarray(buffer = frame0.getBufferByteData(),
                                   dtype = np.uint8,
                                   shape = (frame0.height,
                                            frame0.width,
                                            1))
    cv2.destroyAllWindows()
    max_points = 200 #mostly in case it somehow gets stuck in a loop, and to preallocate the result array
    frame_fit=np.zeros((max_points,2))
    first_frame=True
    widths, convolveresults = [],[]
    test,slices = [], []
    while type(frame) != type(None):
        if display:
            frame_display=frame.copy()
        if direction:
            guess_vector = np.array(directions[direction])
        else:
            raise Exception('Need to define a direction!') #could ask here
            
        if first_frame:
            #TODO try to ID the head?
            #predict tail pos, and dir?
            #then query user for ok
            #detect things aligned the same as before, by looking for big shifts between first frames of video?
            
            #if we don't have a start point, query user for one            
            if type(start_point)==type(np.array([])) or type(start_point) is list:
                current=np.array(start_point)
                point = current
            else:
                handlec=handleclick
                cv2.namedWindow('first')
                cv2.imshow("first",frame)
                cv2.moveWindow('first',0,0)
                #would be nice to raise window, since it doesn't always spawn top
                cv2.waitKey(10)
                point = np.array([-1,-1])
                cv2.setMouseCallback("first",handlec,point)
                print("Click on start of the fish's tail")
                cv2.waitKey(10)
                while (point == np.array([-1,-1])).all():
                    cv2.waitKey(10)
                current = point
                start_point=current
                cv2.destroyWindow('first')
                
            if frame.ndim == 2:
                hist = np.histogram(frame[:,:],10,(0,255))
            elif frame.ndim == 3:
                hist = np.histogram(frame[:,:,0],10,(0,255))
            else:
                raise Exception('Unknown video format!')
            background=hist[1][hist[0].argmax()]/2+hist[1][min(hist[0].argmax()+1,len(hist[0])) ]/2
            #find background - 10 bin hist of frame, use most common as background
            if frame.ndim == 2:
                fish = frame[point[1]-2:point[1]+2,point[0]-2:point[0]+2].mean()
            elif frame.ndim == 3:
                fish = frame[point[1]-2:point[1]+2,point[0]-2:point[0]+2,0].mean()
            #find fish luminosity - area around point
            print("Starting tailfit on: {}".format(filename))
            FPS = 10
            numframes = 10000
            print("FPS is: {} and frame count is: {}".format(FPS, numframes))
            guess_line_width = 51
            normpdf = pylab.normpdf(np.arange((-guess_line_width+1)/4+1,(guess_line_width-1)/4),0,8) #gaussian kernel, used to find middle of tail           
            if display:
                cv2.namedWindow("frame_display")
                cv2.moveWindow("frame_display",0,0)
            starttime = time.perf_counter()
            
        else:
            current= fitted_tail[-1][0,:]

        tailpoint_spacing = 5  #change this multiplier to change the point spacing
        for count in range(max_points):
            
            if count == 0:
                guess = current
            elif count == 1:
                guess = current + guess_vector*tailpoint_spacing #can't advance guess vector, since we didn't move from our previous point
            else:
                guess_vector=guess_vector/(((guess_vector**2).sum())**.5) #normalize guess vector
                guess = current + guess_vector*tailpoint_spacing
            guess_line_start = guess + np.array([-guess_vector[1],guess_vector[0]])*guess_line_width/2
            guess_line_end = guess + np.array([guess_vector[1],-guess_vector[0]])*guess_line_width/2
            x_indices = np.int_(np.linspace(guess_line_start[0],guess_line_end[0],guess_line_width))
            y_indices = np.int_(np.linspace(guess_line_start[1],guess_line_end[1],guess_line_width))

            if max(y_indices) >= frame.shape[0] or min(y_indices) < 0 or max(x_indices) >= frame.shape[1] or min(x_indices) < 0:
                y_indices = np.clip(y_indices,0, frame.shape[0]-1)
                x_indices = np.clip(x_indices,0, frame.shape[1]-1)
                print("Tail got too close to the edge of the frame, clipping search area!")
                #TODO if too many values are clipped, break?
                
            guess_slice= frame[y_indices,x_indices] #the frame is transposed compared to what might be expected
            if guess_slice.ndim == 2:
                guess_slice=guess_slice[:,0]
            else:
                guess_slice=guess_slice[:]
            
            if fish < background:
                guess_slice = (background-guess_slice)
            else:
                guess_slice = (guess_slice-background)

            slices += [guess_slice]
            hist = np.histogram(guess_slice, 10)
            guess_slice = guess_slice-guess_slice[((hist[1][hist[0].argmax()]<=guess_slice)& (guess_slice<hist[1][hist[0].argmax()+1]))].mean()
            #baseline subtraction
            
            sguess = scipy.ndimage.filters.percentile_filter(guess_slice,50,5) #this seems to do a nice job of smoothing out while not moving edges too much
            
            if first_frame:
                #first time through, profile the tail
                tailedges = np.where(np.diff(sguess>(sguess.max()*.25)))[0]
                if len(tailedges)>=2:
                    tailedges = tailedges-len(sguess)/2.0
                    tailindexes = tailedges[np.argsort(np.abs(tailedges))[0:2]]
                    result_index_new = (tailindexes).mean()+len(sguess)/2.0
                    widths +=[abs(tailindexes[0]-tailindexes[1])]
                else:
                    result_index_new = None
                    tail_length = count
                    break
                results= np.convolve(normpdf,guess_slice,"valid")
                convolveresults+=[results]
##                test+=[guess_slice.mean()]
                result_index = results.argmax() - results.size/2+guess_slice.size/2
                newpoint = np.array([x_indices[result_index_new],y_indices[result_index_new]])
                    
            else:
                results= np.convolve(tailfuncs[count],guess_slice,"valid")
                result_index = results.argmax() - results.size/2+guess_slice.size/2
                newpoint = np.array([x_indices[result_index],y_indices[result_index]])
                
            if first_frame:
                if count > 10:
                    #@ SCALE FIT GOODNESS WITH CONTRAST
                    trapz = [pylab.trapz(result-result.mean()) for result in convolveresults]
                    slicesnp = np.vstack(slices)
                    if np.array(trapz[-3:]).mean() < .2:
##                        pdb.set_trace()
                        tail_length = count
                        break

                    elif slicesnp[-1,result_index-2:result_index+2].mean()<10:
##                    elif -np.diff(sliding_average(slicesnp.mean(1)),4).min()<0:

##                    elif np.diff(scipy.ndimage.filters.percentile_filter(trapz,50,4)).min()<-20:
##                        print np.abs(np.diff(trapz))
##                        pdb.set_trace()
                        tail_length = count
                        break
##            elif count > 1 and pylab.trapz(results-results.mean())<.3: #lower means higher contrast threshold
            elif count > tail_length*.8 and np.power(newpoint-current,2).sum()**.5 > tailpoint_spacing*1.5:
##                print count, ' Point Distance Break', np.power(newpoint-current,2).sum()**.5
                break              
            elif count == tail_length: 
                break    #should be end of the tail 
#threshold changes with tail speed?
#also could try overfit, and seeing where the elbow is

            if display:
                cv2.circle(frame_display,(int(newpoint[0]),int(newpoint[1])),2,(0,0,0))
##                frame_display[y_indices,x_indices]=0

            frame_fit[count,:]=newpoint
                           
            if count>0:
                guess_vector = newpoint-current
            current = newpoint
            #@ autoscale guess line width and then regen normpdf
##        trapz = [pylab.trapz(result-result.mean()) for result in convolveresults]
##        pylab.scatter(range(len(trapz)),trapz)                  
##        pylab.figure()
##        td = sliding_average(np.abs(np.diff(trapz)),5)
##        
##        pylab.scatter(range(len(td)),td,c='r');pylab.show()
##        pylab.axhline()

##        pylab.plot(trapz)
####        pylab.plot(np.abs(np.diff(scipy.ndimage.filters.percentile_filter(trapz,50,4))))
##        pylab.plot(scipy.ndimage.filters.percentile_filter(trapz,50,4))
##        pylab.plot(test)
####        pylab.plot(slices
##        slices = np.vstack(slices)
##        pylab.show()
##
##        pylab.plot(sliding_average(slices.mean(1)));
##        pylab.plot(np.abs(np.diff(sliding_average(slices.mean(1),8))));
##        pylab.plot(-np.diff(sliding_average(slicesnp.mean(1)))[:45]);pylab.show()
##        pdb.set_trace()
        if first_frame:
            swidths = scipy.ndimage.filters.percentile_filter(widths,50,8)
            swidths = np.lib.pad(swidths,[0,5],mode='edge')
            tailfuncs = [ tail_func2(np.arange((-guess_line_width+1)/4+1,(guess_line_width-1)/4),0, swidth, 1, 0) for swidth in swidths]
             
        fitted_tail.append(np.copy(frame_fit[:count]))
        if display:
            cv2.putText(frame_display,str(count),(340,25),cv2.FONT_HERSHEY_SIMPLEX, 1.0,(225,10,20) );
            cv2.putText(frame_display,str(len(fitted_tail)-1),(15,25),cv2.FONT_HERSHEY_SIMPLEX, 1.0,(25,10,20) ); #-1 because the current frame has already been appended
            cv2.imshow("frame_display",frame_display)
            if first_frame:
                delaytime = 1
            else:
                minlen = min([fitted_tail[-2].shape[0],fitted_tail[-1].shape[0]])-1
                delaytime = int(min(max((np.abs((fitted_tail[-2][minlen,:]-fitted_tail[-1][minlen,:])**2).sum()**.5)**1.2*3-1,1), 500))
##            print delaytime
            cv2.waitKey(delaytime)

        if output_jpegs:
            if first_frame:
                jpegs_dir = pickdir()
                if not os.path.exists(jpegs_dir):
                    os.makedirs(jpegs_dir)
            jpg_out = Image.fromarray(frame_display)
            jpg_out.save(os.path.normpath(jpegs_dir +'\\'+ str(len(fitted_tail)-1)+'.jpg'))
            
        first_frame = False
        ##        cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,float(len(fitted_tail)) );  #workaround for raw videos crash, but massively (ie 5x) slower
        frame = cap.read()[1]
    print("Done in {:.2f} seconds".format(time.perf_counter()-starttime))

    fit_lengths = np.array([len(i) for i in fitted_tail])    
    if np.std(fit_lengths) > 3 or plotlengths:
        print('Abnormal variances in tail length detected, check results: {}'.format(filename))

        pylab.plot(range(0,len(fitted_tail)),fit_lengths)
        pylab.ylim((0,5+max(fit_lengths)))
        pylab.xlabel("Frame")
        pylab.ylabel('Tail points')
        pylab.title('Tail fit lengths')
        print('Close graph to continue!')
        pylab.show()
        
    if any(fit_lengths<25):
        print("Warning - short tail detected in some frames - min: {}".format(min(fit_lengths)))

    if len(fitted_tail) != int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)):
        print("Warning - number of frames processed doesn't match number of video frames - can happen with videos over 2gb!")
        print("Frames processed: {}".format(len(fitted_tail)))
        print("Actual frames according to video header: {}".format(int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))))
    cv2.destroyAllWindows()
    return fitted_tail, start_point, direction, FPS, numframes

def tailfit_batch(video_list=[], display=True, displayonlyfirst = True,shelve_path = r'default.shv', startpoints=None, reuse_startpoint = False):
    shv=shelve.open(shelve_path,writeback=False)
    first = True
    for i, videopath in enumerate(video_list):
        if type(startpoints) is list:
            fittedtail,startpoint,  direction, FPS, numframes  = tailfit(videopath,(first or not displayonlyfirst) and display ,startpoints[i])
        else:
            fittedtail,startpoint,  direction, FPS, numframes  = tailfit(videopath,(first or not displayonlyfirst) and display ,startpoints)
            if reuse_startpoint:
                startpoints = [startpoint]*len(video_list)
        shahash = hashfile(open(videopath, 'rb'), hashlib.sha256())
        
        result =  tailfitresult(fittedtail, str(os.path.basename(videopath)),videopath, startpoint, FPS, len(fittedtail), direction, shahash, __version__)
        shv[str(os.path.basename(videopath))]=result
        shv.sync()
        if first:
            first = False
    shv.close()   


if __name__ == "__main__":
##    tail=tailfit('12-03-20.428.avi',display=1)
##    tail=tailfit(pickfile(),display=askyesno(text='Display frames?'),output_jpegs = 0)
##    tail=tailfit('test_tailfit.avi',display=1,output_jpegs = 0, start_point = [138, 176])

    filenames = pickfiles(filetypes = [("AVI Video", ("*.avi", )), ("Shelve files",'*.shv'), ('All','*') ])
    if all([f.endswith('.shv') for f in filenames]):
        output_suffix = ''
        for shvpath in [avi for avi in filenames if avi.endswith('.shv')]:

            shv = shelve.open(shvpath)
            basenames = shv.keys()
            if type(shv[basenames[0]]) is tailfitresult:
                startpoints = [shv[j].startpoint for j in shv.keys()]
            elif type(shv[basenames[0]]) is list:
                startpoints = [shv[j][1] for j in shv.keys()]
            else:
                raise Exception("Unknown tailfit format!")
            
            shv.close()

            print('Running from: {}'.format(os.path.basename(shvpath)))
            print('Filenames: {}'.format(' '.join(basenames)))
            
            basedir = pickdir()
            avis = [os.path.join(basedir,avi) for avi in basenames]
            assert all([os.path.exists(avi) for avi in avis]), "Exiting, couldn't find matching avi file for all files!"
            outputshv = saveasfile(filetypes = [("Shelve files",'*.shv'), ('All','*') ],defaultextension='.shv')
            print()
            tailfit_batch(avis,False,False,outputshv,startpoints=startpoints)
            
    else:
        
        avis = [avi for avi in filenames if avi.endswith('.avi')]
        assert len(avis) == len(filenames), 'You need to select all only AVIs or all only shelve files'
        
        outputshv = saveasfile(filetypes = [("Shelve files",'*.shv'), ('All','*') ],defaultextension='.shv')
        display=askyesno(text='Display frames?')
        displayonlyfirst = True
        if display and len(avis)>1:
            displayonlyfirst=askyesno(text='Display only first video?')
        tailfit_batch(avis,display,displayonlyfirst,outputshv, reuse_startpoint = False) #this is where you change if you want to reuse the startpoint

##    cProfile.run("tailfit('20-39-04.643.avi',display=0)",'profile_tail9',)
##    import pstats
##    p = pstats.Stats('profile_tail9')
##    p.sort_stats('cumulative').print_stats(30)
    
#tailfit function todos:
#better lightning optimiziation? - is this still needed?
#detects motion between videos for batches (if so, requery point)
#smart detect when it needs to have that glitchy setpos fix
    
    
