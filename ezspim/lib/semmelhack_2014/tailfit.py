import cv2
from datetime import datetime
import pylab
import numpy as np
import time
import scipy.ndimage
import scipy.stats
import threading


class TailFit:

    background = None
    fish = None
    guess_line_width = 60 # used to be 120.
    scaledown = 0.5
    #max_points = 12 # USE 12 for full tail
    max_points = 10 # 
    tailpoint_spacing = 40 # used to be 60.
    expected_points = 6 # number of expected points
    displayfreq = 10
    displaydur = 50 #ms
    normpdf = scipy.stats.norm.pdf(np.arange((-guess_line_width+1)/4+1,(guess_line_width-1)/4),0,8) #gaussian kernel, used to find middle of tail
    first_frame=True;
    guess_vector = np.array([0,1])
    slices=[]
    widths=[]
    convolveresults=[]
    tail_length = None
    running = True
    drawlock = threading.Lock();
    drawdata = None
    cb = None
    displayi = 0
    

    def __init__(self,callback):
        self.cb = callback
        threading.Thread(target=self.display).start()
        return

    def handleclick(self,event,x,y,flags,param):
        if event==cv2.EVENT_LBUTTONDOWN:
            self.running = False
        self.cb(event,x,y,flags,param)

    def display(self):
        while True:
            if not self.running:
                return
            frame = self.drawdata
            self.drawdata = None
            if frame is None:
                time.sleep(0.01)
                continue
            n = datetime.now()
            t = n.timetuple()
            t_y, t_m, t_d, t_h, t_min, t_sec, t_wd, t_yd, t_i = t
            cv2.putText(frame,str(t_y)+"-"+str(t_m)+"-"+str(t_d)+" "+str(t_h)+":"+str(t_min)+":"+str(t_sec),(0,frame.shape[0]-12), cv2.FONT_HERSHEY_SIMPLEX, 0.3,(255,255,255),1)
            cv2.putText(frame,"Startpoint",(int(self.startx/self.scaledown+10),int(self.starty/self.scaledown+12)), cv2.FONT_HERSHEY_SIMPLEX, 0.3,(0,0,255),1)
            cv2.circle(frame,(int(self.startx/self.scaledown),int(self.starty/self.scaledown+5)), 3, (0,0,255), -1)
            cv2.imshow("Tail",frame)
            cv2.setMouseCallback("Tail",self.handleclick,np.array([-1,-1]))
            cv2.waitKey(self.displaydur)
    
    def recalcMetrics(self):
        self.first_frame=True;
        
    def tail_func2(self,x, mu, sigma, scale, offset):
        return scale * np.exp(-(x-mu)**4/(2.0*sigma**2))**.2 + offset

    def tailfit(self,frame,display=True,start_point=None):
        frame=cv2.resize(frame, (int(frame.shape[1]/2), int(frame.shape[0]/2)))
        alltailpoints = []
        starttime = time.perf_counter()
        if display:
            frame_display=cv2.resize(frame, (int(frame.shape[1]/self.scaledown), int(frame.shape[0]/self.scaledown)))
            frame_display=cv2.cvtColor(frame_display, cv2.COLOR_GRAY2RGB)

        if self.first_frame:
            self.guess_vector = [0,1];
            hist = np.histogram(frame[:,:],15,(0,150))
            self.startx=frame.shape[1]/2
            self.starty=0
            self.startpoint=np.array([self.startx, self.starty],dtype='int')
            self.tailpoint=np.array([self.startx, self.starty],dtype='int')
            
            point = self.startpoint;
            point[1] = 20
            self.background = hist[1][hist[0].argmax()]/2+hist[1][min(hist[0].argmax()+1,len(hist[0])) ]/2
            self.fish = frame[point[1]-10:point[1]+10,point[0]-10:point[0]+10].mean()
            print(self.background)
            print(self.fish)
            print(self.tailpoint)
            
        self.guess_vector = np.array([0,1]);
        for count in range(self.max_points):
            self.guess_vector=np.array(self.guess_vector)
            if count == 0:
                guess = self.tailpoint
            elif count == 1:
                #can't advance guess vector, since we didn't move from our previous point
                guess = self.tailpoint + self.guess_vector*self.tailpoint_spacing 
            else:
                self.guess_vector=self.guess_vector/(((self.guess_vector**2).sum())**.5) #normalize guess vector
                guess = current + self.guess_vector*self.tailpoint_spacing
            guess_line_start = guess + np.array([-self.guess_vector[1],self.guess_vector[0]])*self.guess_line_width/2
            guess_line_end = guess + np.array([self.guess_vector[1],-self.guess_vector[0]])*self.guess_line_width/2
            x_indices = np.int_(np.linspace(guess_line_start[0],guess_line_end[0],self.guess_line_width))
            y_indices = np.int_(np.linspace(guess_line_start[1],guess_line_end[1],self.guess_line_width))

            if max(y_indices) >= frame.shape[0] or min(y_indices) < 0 or max(x_indices) >= frame.shape[1] or min(x_indices) < 0:
                y_indices = np.clip(y_indices,0, frame.shape[0]-1)
                x_indices = np.clip(x_indices,0, frame.shape[1]-1)
                #print("Tail got too close to the edge of the frame, clipping search area!")
                
            guess_slice= frame[y_indices,x_indices] #the frame is transposed compared to what might be expected
            #if guess_slice.ndim == 2:
            #    guess_slice=guess_slice[:,0]
            #else:
            guess_slice=guess_slice.flatten()
            if self.fish < self.background:
                guess_slice = (self.background-guess_slice)
            else:
                guess_slice = (guess_slice-self.background)

           
            if self.first_frame:
                self.slices += [guess_slice]
            hist = np.histogram(guess_slice, 15)
            guess_slice = guess_slice-guess_slice[((hist[1][hist[0].argmax()]<=guess_slice)& (guess_slice<hist[1][hist[0].argmax()+1]))].mean()
     
            #baseline subtraction
            
            sguess = scipy.ndimage.filters.percentile_filter(guess_slice,50,10) #this seems to do a nice job of smoothing out while not moving edges too much
            
            if self.first_frame:
                #first time through, profile the tail
                tmpprofile=sguess>(sguess.max()*.25)
                tmpprofile=tmpprofile.flatten()
                tailedges = np.where(np.diff(tmpprofile.astype(int)))[0]
                if len(tailedges)>=2:
                    tailedges = tailedges-len(sguess)/2.0
                    tailindexes = tailedges[np.argsort(np.abs(tailedges))[0:2]]
                    result_index_new = int((tailindexes).mean()+len(sguess)/2.0)
                    self.widths +=[abs(tailindexes[0]-tailindexes[1])]
                    self.tail_length = count
                else:
                    result_index_new = None
                    self.tail_length = count
                    break
                results= np.convolve(self.normpdf,guess_slice,"valid")
                self.convolveresults+=[results]
                result_index = int(results.argmax() - results.size/2+guess_slice.size/2)
                newpoint = np.array([x_indices[result_index_new],y_indices[result_index_new]])
                    
            else:
                results= np.convolve(self.tailfuncs[count],guess_slice,"valid")
                result_index = int(results.argmax() - results.size/2+guess_slice.size/2)
                newpoint = np.array([x_indices[result_index],y_indices[result_index]])
                
            if self.first_frame:
                if count > self.expected_points:  
                    #@ SCALE FIT GOODNESS WITH CONTRAST
                    trapz = [pylab.trapz(result-result.mean()) for result in self.convolveresults]
                    slicesnp = np.vstack(self.slices)
                    if np.array(trapz[-3:]).mean() < .2:
                        self.tail_length = count
                        break

                    elif slicesnp[-1,result_index-2:result_index+2].mean()<10:
                        self.tail_length = count
                        break
            elif count > self.tail_length*.8 and np.power(newpoint-current,2).sum()**.5 > self.tailpoint_spacing*1.5:
                break              
            elif count == self.tail_length: 
                break 
            #print(newpoint)
            if display:
                cv2.circle(frame_display,(int(newpoint[0]/self.scaledown),int(newpoint[1]/self.scaledown)),3,(0,255,0),-1)

            #frame_fit[count,:]=newpoint
                           
            if count>0:
                self.guess_vector = newpoint-current
            current = newpoint
            alltailpoints.append(current.copy())
            
        if self.first_frame and len(self.widths) != 0:
            swidths = scipy.ndimage.filters.percentile_filter(self.widths,50,8)
            swidths = np.lib.pad(swidths,[0,5],mode='edge')
            self.tailfuncs = [ self.tail_func2(np.arange((-self.guess_line_width+1)/4+1,(self.guess_line_width-1)/4),0, swidth, 1, 0) for swidth in swidths]       
            self.first_frame=False;
        #fitted_tail.append(np.copy(frame_fit[:count]))
        #print(frame_fit)
        endtime = time.perf_counter()
        if display and self.displayi % self.displayfreq == 0:
            frame_display *= 2
            cv2.putText(frame_display,"TF: Compute time " + str((endtime-starttime)*1000) + "ms",(0,int(frame.shape[0]/self.scaledown-6)), cv2.FONT_HERSHEY_SIMPLEX, 0.3,(0,0,255),1)
            #if self.drawlock.acquire(False):
            self.drawdata = frame_display
                #self.drawlock.release()
        elif self.displayi % self.displayfreq == 0:
            pass
        self.displayi += 1
        return alltailpoints
