import cv2
from pymba import *
import numpy as np
from ..lib.semmelhack_2014.tailfit import TailFit
from multiprocessing import Pipe
import multiprocessing
import threading
import time
import psutil
import atexit
import signal
import pickle

def ch_slavelauncher(child_conn_ctrl, child_conn_data, fit_tail=True, width=320, height=256,showcam=True):
    p = psutil.Process()
    p.cpu_affinity([0,1])
    p.nice(psutil.HIGH_PRIORITY_CLASS)
    CameraHandler.child_conn_ctrl = child_conn_ctrl
    CameraHandler.child_conn_data = child_conn_data
    CameraHandler.initParams(fit_tail, width, height, showcam)
    #print(CameraHandler.fit_tail)
    CameraHandler.slave()

class CameraHandler:

    @staticmethod
    def init(fit_tail=True,tf_callback=None, width=320, height=256,showcam=True):
        CameraHandler.initParams(fit_tail, width, height, showcam)
        CameraHandler.callback = tf_callback
        
        CameraHandler.parent_conn_ctrl, CameraHandler.child_conn_ctrl = Pipe()
        CameraHandler.parent_conn_data, CameraHandler.child_conn_data = Pipe()
        CameraHandler.proc = multiprocessing.Process(target=ch_slavelauncher, 
                                                     args=[CameraHandler.child_conn_ctrl, CameraHandler.child_conn_data, fit_tail, width, height, showcam]);
        #TODO: set daemon?
        CameraHandler.proc.start();
            
        CameraHandler.wd = threading.Thread(target=CameraHandler.watchdog)
        CameraHandler.wd.start();
        time.sleep(3)
        
    @staticmethod
    def initParams(fit_tail=True, width=320, height=256,showcam=True):
        CameraHandler.w = width
        CameraHandler.h = height
        CameraHandler.showcam = showcam
        CameraHandler.frame_id = 0
        CameraHandler._halt = False
        CameraHandler.vout = None
        CameraHandler.fit_tail = fit_tail


    @staticmethod
    def slave():
        def handleclick(event,x,y,flags,param):
            return
        if CameraHandler.fit_tail:
            CameraHandler.tf = TailFit(handleclick);
        CameraHandler.vimba = Vimba()
        CameraHandler.vimba.startup()
        CameraHandler.system = CameraHandler.vimba.getSystem()
        CameraHandler.cameraIds = CameraHandler.vimba.getCameraIds()
        #for cameraId in cameraIds:
        #    print(cameraId)
        CameraHandler.camera = CameraHandler.vimba.getCamera(CameraHandler.cameraIds[0])
        CameraHandler.camera.openCamera()
        #print(camera0.AcquisitionMode)
        def haltp():
            print("HALTING PYMBA")
            CameraHandler._halt = True
            CameraHandler.camera.endCapture()
            CameraHandler.camera.revokeAllFrames()
            CameraHandler.camera.runFeatureCommand('AcquisitionStop')
            CameraHandler.child_conn_data.send(['h']);
        atexit.register(haltp)
        signal.signal(signal.SIGTERM, haltp)
        signal.signal(signal.SIGINT, haltp)
        while True:
            payload = CameraHandler.child_conn_ctrl.recv()
            if payload[0] == 'h':
                print("Halting Camera Handler")
                CameraHandler._halt = True
                CameraHandler.camera.endCapture()
                CameraHandler.camera.revokeAllFrames()
                CameraHandler.camera.runFeatureCommand('AcquisitionStop')
                CameraHandler.child_conn_data.send(['h']);
                return
            elif payload[0] == 's':
                print("Starting Camera Handler")
                CameraHandler.framecapture = CameraHandler.camera.getFrame()    # creates a frame
                CameraHandler.framecapture.announceFrame()
                CameraHandler.camera.startCapture()
                CameraHandler.framecapture.queueFrameCapture(CameraHandler.__onFrameCapture);
                CameraHandler.camera.runFeatureCommand('AcquisitionStart')
            elif payload[0] == 'sOF':
                CameraHandler.vout = cv2.VideoWriter(payload[1], cv2.VideoWriter_fourcc(*'IYUV'), 100.0, (CameraHandler.w,CameraHandler.h))
    
    @staticmethod
    def watchdog():
        while True:
            payload = CameraHandler.parent_conn_data.recv()
            if payload[0] == 'tp':
                #print("WATCHDOG DATA")
                if CameraHandler.callback is not None:
                    CameraHandler.callback(payload[1])
            elif payload[0] == 'fid':
                #print("Watchdog: {:.20f}".format(time.time()))
                CameraHandler.frame_id = payload[1]
            elif payload[0] == 'h':
                return


    @staticmethod
    def describe():
        return {'FrameId':CameraHandler.frame_id}

    @staticmethod
    def start():
        CameraHandler.parent_conn_ctrl.send(['s']);

    @staticmethod
    def setOutputFile(filename):
        """
        Sets file to which the frames are written

        Args:
            filename (str): Name of the file to which the frames are saved
        """
        CameraHandler.parent_conn_ctrl.send(['sOF', filename]);

    @staticmethod
    def setCallback(callback_method):
        """
        Sets function that is called when new frame arrives, tail points are supplied to that method

        Args:
            callback_method (funciton): Function which is called on new frame
        """
        CameraHandler.callback = callback_method

    @staticmethod
    def halt():
        """
        Stops the camera handler
        """
        CameraHandler.parent_conn_ctrl.send(['h']);


    @staticmethod
    def __onFrameCapture(framecapture):
        if CameraHandler._halt:
            return
        frame = np.ndarray(buffer = framecapture.getBufferByteData(),
                            dtype = np.uint8,
                            shape = (framecapture.height,
                                    framecapture.width,
                                    1))
        #if CameraHandler.vout is not None: #DISABLED
        #    CameraHandler.vout.write(cv2.resize(frame, (CameraHandler.w, CameraHandler.h)) )
            
        frame = cv2.resize(frame, (int(framecapture.width/2), int(framecapture.height/2)))
        #pickle.dump(frame,open("{}_frame.p".format(CameraHandler.frame_id),'wb'))

        if CameraHandler.showcam:
            cv2.imshow("Tail",frame)
            cv2.waitKey(1)
        CameraHandler.frame_id+=1
        #print("Slave: {:.20f}".format(time.time()))
        #print("TAILPOINTS")
        CameraHandler.child_conn_data.send(['fid', CameraHandler.frame_id]);
        if CameraHandler.fit_tail:
            tailpoints = CameraHandler.tf.tailfit(np.swapaxes(frame,0,1))
            CameraHandler.child_conn_data.send(['tp', tailpoints]);
        framecapture.queueFrameCapture(CameraHandler.__onFrameCapture);