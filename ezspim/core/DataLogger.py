
from datetime import datetime, timedelta
#import ujson
import json
from multiprocessing import Pipe
import multiprocessing
import threading
import time
import psutil
import numpy as np
from bson import objectid
import sys

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

class DataLogger:
    base_dir = ".\\"
    def __init__(self,filename, id = None, use_global_dir = True):
        """
        Initialize data logger with the name of the file to which the data will be written

        Args:
            filename (str): Name of the data file
        """
        if use_global_dir:
            self.fname = DataLogger.base_dir+filename
        else:
            self.fname = filename
        self.started = False
        if id is None:
            id = objectid.ObjectId()
        self.id = id

    def init(self):
        self.start_time = time.perf_counter()
        self.parent_conn, self.child_conn = Pipe()
        self.started = True
        self.data = []
        self.proc = multiprocessing.Process(target=self.slave, args=[self.id])
        #TODO: set daemon?
        self.proc.start();

    def slave(self, id):
        p = psutil.Process()
        p.cpu_affinity([3,4,5])
        p.nice(psutil.REALTIME_PRIORITY_CLASS)
        while True:
            payload = self.child_conn.recv()
            if payload[0] == 'wD': #writeDatapoint: datapoint
                self.data.append(payload[1])
            elif payload[0] == 'aV': #addValue: key,value
                if len(self.data) == 0: self.data.append({})
                self.data[-1][payload[1]] = payload[2]
            elif payload[0] == 'nD': #newDatapoint
                self.data.append({})
            elif payload[0] == 'f': #Flush
                self.f = open(self.fname, "w")
                data = {'data':self.data, 
                        'date': datetime.now().isoformat(),
                        'id': str(id),
                        'version':"1.0.0"}
                start = time.perf_counter()
                json.dump(data, self.f, cls=NumpyEncoder)#,indent=2
                self.f.close()
                end = time.perf_counter()
                elapsed = end - start
                print("Saving complete, elapsed={}".format(elapsed)) 
            elif payload[0] == 'c': #Close
                #p.kill()
                return




    def writeDatapoint(self,datapoint):
        """
        Appends datapoint provided to the list of datapoints to be logged

        Args:
            datapoint (dict): Dictionary of the name/value pairs for the current data point

        Returns:
            self: Use to chain methods when appropriate
        """
        if not self.started:
            self.init()
        self.parent_conn.send(['wD', datapoint]);
        return self

    
    def addValue(self,key,value):
        """
        Adds key/value pair to the last datapoint

        Args:
            key (str): Key for the key/value pair
            value (str): Value for the key/value pair

        Returns:
            self: Use to chain methods when appropriate
        """
        if not self.started:
            self.init()
        #print(sys.getsizeof(value))
        #regime_start = time.perf_counter()
        self.parent_conn.send(['aV', key, value,"fuckthisfu"*1000]);
        #regime_timestamp = time.perf_counter() - regime_start
        #print("TTT:{}".format(regime_timestamp))
        return self

    def newDatapoint(self):
        """
        Appends a new empty datapoint to the list of datapoints to be logged

        Returns:
            self: Use to chain methods when appropriate
        """
        if not self.started:
            self.init()
        self.parent_conn.send(['nD']);
        return self

    def flush(self):
        """
        Flush all datapoints into the data file
        """
        if not self.started:
            self.init()
        self.parent_conn.send(['f']);

    def close(self):
        """
        Close the data logger (without saving to disk)
        """
        if not self.started:
            self.init()
        self.parent_conn.send(['c']);
        
