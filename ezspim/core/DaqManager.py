from ..lib.daq.pyDAQmxReadCounter import frameCounter
from ..lib.daq.pyDAQmxReadAI import ReadDAQmxAI

class DaqManager:
    def __init__(self,analogue_channel='AI1'):
        self.aiReader = ReadDAQmxAI(samples_to_read=10000, volt_range=[-0.05, 3.5],analogue_channel=analogue_channel);
        self.counter = frameCounter()
        self.frameCount = self.counter.readCounter()[0]

    def block(self):
        while self.counter.waitForFrameCounter()[0] == 0:
            print("Waiting for signal")
            continue

    def getFrameCount(self):
        return self.counter.readCounter()[0];

    def close(self):
        self.counter.close()