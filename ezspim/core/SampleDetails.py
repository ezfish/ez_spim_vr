from datetime import datetime
from .DataLogger import DataLogger

class SampleDetails:
    def __init__(self, sample_id, dpf, expression, description):
        self.sample_id = sample_id
        self.dpf = dpf
        self.expression = expression
        self.description = description
    
    def describe(self):
        meta_dict = {
            "sample_id": self.sample_id,
            "description": self.description,
            "expression": self.expression,
            "dpf": self.dpf
            }
        return meta_dict