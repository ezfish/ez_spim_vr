from ezspim.core.DataLogger import DataLogger
from datetime import datetime
import time


class Battery():
    def __init__(self, dir, sample_details=None, name="Battery", description=""):
        self.experiments = []
        self.dir = dir
        self.name = name
        self.description = description

    def append(self, experiment):
        self.experiments.append(experiment);

    def run(self):
        expid = []
        for e in self.experiments:
            expid.append(str(e.id))
        date = datetime.now().isoformat().split('.')[0].replace(':','');
        meta_dict = {
            "type": "Battery",
            "name": self.name,
            "description": self.description,
            "experiments": expid
            }
        log = self.dir + '\\{}_battery{}.json'.format(date,self.name)
        logger = DataLogger(log)
        logger.writeDatapoint(meta_dict)
        for e in self.experiments:
            e.run()
        logger.flush()
        logger.close()