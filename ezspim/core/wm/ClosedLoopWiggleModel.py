import numpy as np
import collections
import time
import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

class ClosedLoopWiggleModel:
    """Wiggle Wiggle Wiggle!"""

    # new
    def __init__(self, tau_A=1, tau_V=0.3, gamma=1, gain = 1, print_interval = 100):
        self.tau_A = tau_A
        self.tau_V = tau_V
        self.gamma = gamma
        self.gain = gain
        self.velocity = 0.0
        self.self_velocity = 0.0
        self.env_velocity = 0.0
        self.acceleration = 0.0
        self.position = 0.0
        self.last_tailangle = 0
        self.tailchange = 0
        self.tailchange_accum = 0
        self.tailpoints = []
        self.print_interval = print_interval
        self.since_last_print = 0
        
        self.draw_velocity = collections.deque(maxlen=600)
        self.draw_acceleration = collections.deque(maxlen=600)
        self.draw_tailchange = collections.deque(maxlen=600)
        self.draw_time = collections.deque(maxlen=600)

        self.fig = None;

    def enable_plotting(self):        
        self.fig = plt.figure()
        self.fig.show()
        self.ax1 = self.fig.add_subplot(1, 1, 1)
        self.ax1.cla()
        self.ax1.set_title('Velocity')
        self.ax1.set_xlabel('Time (s)')
        self.ax1.set_ylabel('Velocity (mm/s)')

        plt.ion()  # Set interactive mode ON, so matplotlib will not be blocking the window
        plt.show(False)  # Set to false so that the code doesn't stop here

        self.ax1.hold(True)
        self.line1, = self.ax1.plot(self.draw_time, self.draw_velocity, '.-', alpha=0.8, color="gray", markerfacecolor="red")
        
        self.fig.canvas.draw()
        self.background = self.fig.canvas.copy_from_bbox(self.ax1.bbox) # cache the background


     # new
    def loop(self, timedelta):
        #print("{:.10f}".format(timedelta))
        self.tailchange_accum += self.tailchange*timedelta

        D_Acc = (-self.acceleration - self.tailchange*self.gain) / self.tau_A
        D_Vel = ((- self.gamma * self.self_velocity) + self.acceleration) / self.tau_V
        D_Pos = self.velocity

        self.acceleration = D_Acc * timedelta + self.acceleration
        self.self_velocity = self.self_velocity + D_Vel *timedelta
        self.velocity = self.self_velocity + self.env_velocity
        self.position = D_Pos * timedelta + self.position
        
        if self.fig is not None:
            self.draw_velocity.append(self.velocity)
            self.draw_acceleration.append(self.acceleration)
            self.draw_tailchange.append(self.tailchange)
            self.draw_time.append(time.perf_counter());
        
            #PLOT
            self.line1.set_xdata(self.draw_time)
            self.line1.set_ydata(self.velocity)
            plt.axis([min(self.draw_time) / 1.05, max(self.draw_time) * 1.1, 0, 20])
            self.fig.canvas.restore_region(self.background)    # restore background
            self.ax1.draw_artist(self.line1)                   # redraw just the points
            self.fig.canvas.blit(self.ax1.bbox)                # fill in the axes rectangle
            self.fig.canvas.flush_events()


        #print('Grating: {:.1f}mm/s, Velocity: {:.2f}mm/s, Position: {:.2f}mm'.format(self.env_velocity, self.self_velocity, self.position))
        if self.since_last_print >= self.print_interval:
            print('Grating: {:.1f}mm/s, Gain: {:.1f}, TailChange: {:.2f}, Position: {:.2f}mm, dt: {:.5f}'.format(self.env_velocity, self.gain, self.tailchange, self.position, timedelta))
            self.since_last_print = 0
        self.since_last_print += 1
        #print('Ev: {:.1f}, Bv: {:.3f}, A: {:.2f}, V: {:.3f}, P:{:.2f}, TCA:{:.5}'.format(self.env_velocity, self.self_velocity, self.acceleration, self.velocity, self.position, self.tailchange_accum))
        
    def describe(self):
        return {'Params': 
                {
                    "tau_A": self.tau_A, 
                    "tau_V": self.tau_V, 
                    "gamma": self.gamma,
                    "gain": self.gain
                    },
                'Velocity': float(self.velocity),
                'VelocitySelf': float(self.self_velocity),
                'VelocityEnv': float(self.env_velocity),
                'Acceleration': float(self.acceleration),
                'Position': float(self.position),
                'TailChange': float(self.tailchange),
                'TailChangeAccum': float(self.tailchange_accum),
                'TailPoints': self.tailpoints,
                'Mode': 'Closed'}

    def setVelocity(self, x):
        self.env_velocity = x
        return self

    def setAcceleration(self, acceleration):
        self.acceleration = acceleration
        return self

    def setPosition(self, position):
        self.position = position
        return self

    def processTailpoints(self, tailpoints):
        self.tailpoints = list(tailpoints)
        fp = np.array(tailpoints[0:4])
        lp = np.array(tailpoints[-3:])
        curve = lp.mean(axis=0) - fp.mean(axis=0)
        ratio = curve[0] / curve[1]
        tailangle = np.absolute(np.rad2deg(np.arctan(ratio)))
        
        tailchange = 12*np.tanh((max(abs(tailangle - self.last_tailangle) - 0.7, 0))/6)
 #       tailchange = (max(abs(tailangle - self.last_tailangle) - 0.7, 0))
        self.last_tailangle = tailangle
        self.tailchange = tailchange


