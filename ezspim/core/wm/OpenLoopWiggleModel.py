
class OpenLoopWiggleModel:
    """Wiggle Wiggle Wiggle!"""

    def __init__(self):
        self.velocity = 0.0
        self.acceleration = 0.0
        self.position = 0.0

    def describe(self):
        return {'Velocity': float(self.velocity),
                'Acceleration': float(self.acceleration),
                'Position': float(self.position),
                'Mode': 'Open'}

    def loop(self, timedelta):
        velocity = self.velocity + self.acceleration * timedelta
        position = self.position + (self.velocity+velocity)/2 * timedelta
        self.velocity = velocity
        self.position = position
        
    def setVelocity(self, velocity):
        self.velocity = velocity
        return self
    def setAcceleration(self, acceleration):
        self.acceleration = acceleration
        return self
    def setPosition(self, position):
        self.position = position
        return self