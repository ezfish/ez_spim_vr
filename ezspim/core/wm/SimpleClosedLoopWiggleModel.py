import numpy as np
import collections
import time
import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

class SimpleClosedLoopWiggleModel:
    """Wiggle Wiggle Wiggle!"""

    # new
    def __init__(self, tau_accum=0.025, tau_decay=0.1, normalizer =5000, gain = 1, print_interval = 100):
        self.gain = gain
        self.tau_accum = tau_accum
        self.tau_decay = tau_decay
        self.normalizer = normalizer
        
        self.print_interval = print_interval
        self.since_last_print = 0
        
        self.acceleration = 0.0
        self.velocity = 0.0
        self.self_velocity = 0.0
        self.env_velocity = 0.0
        self.position = 0.0

        self.last_tailangle = 0
        self.tailchange = 0
        self.tailchange_accum = 0
        self.tailpoints = []
        
        self.draw_velocity = collections.deque(maxlen=600)
        self.draw_acceleration = collections.deque(maxlen=600)
        self.draw_tailchange = collections.deque(maxlen=600)
        self.draw_time = collections.deque(maxlen=600)

        self.fig = None;

    def enable_plotting(self):        
        self.fig = plt.figure()
        self.fig.show()
        self.ax1 = self.fig.add_subplot(1, 1, 1)
        self.ax1.cla()
        self.ax1.set_title('Velocity')
        self.ax1.set_xlabel('Time (s)')
        self.ax1.set_ylabel('Velocity (mm/s)')

        plt.ion()  # Set interactive mode ON, so matplotlib will not be blocking the window
        plt.show(False)  # Set to false so that the code doesn't stop here

        self.ax1.hold(True)
        self.line1, = self.ax1.plot(self.draw_time, self.draw_velocity, '.-', alpha=0.8, color="gray", markerfacecolor="red")
        
        self.fig.canvas.draw()
        self.background = self.fig.canvas.copy_from_bbox(self.ax1.bbox) # cache the background


     # new
    def loop(self, timedelta):
        
        self.tailchange_accum += self.tailchange*timedelta
        D_Acc = -self.acceleration / self.tau_accum - self.tailchange
        D_Vel = -self.self_velocity / self.tau_decay + self.acceleration * self.gain * self.normalizer
        D_Pos = self.velocity
        
        self.acceleration = self.acceleration + D_Acc * timedelta
        self.self_velocity = self.self_velocity + D_Vel *timedelta
        self.velocity = self.self_velocity + self.env_velocity
        self.position = D_Pos * timedelta + self.position

        if self.fig is not None:
            self.draw_velocity.append(self.velocity)
            self.draw_acceleration.append(self.acceleration)
            self.draw_tailchange.append(self.tailchange)
            self.draw_time.append(time.perf_counter());
        
            #PLOT
            self.line1.set_xdata(self.draw_time)
            self.line1.set_ydata(self.velocity)
            plt.axis([min(self.draw_time) / 1.05, max(self.draw_time) * 1.1, 0, 20])
            self.fig.canvas.restore_region(self.background)    # restore background
            self.ax1.draw_artist(self.line1)                   # redraw just the points
            self.fig.canvas.blit(self.ax1.bbox)                # fill in the axes rectangle
            self.fig.canvas.flush_events()

        if self.since_last_print >= self.print_interval:
            print('Gr: {:.1f}mm/s, Ga: {:.1f}, TC: {:.2f}, P: {:.2f}mm, V: {:.2f}mm/s, tail: {:.5f}'.format(self.env_velocity, self.gain, self.tailchange, self.position, self.self_velocity, self.last_tailangle))
            #print('Gr: {:.1f}mm/s, Ga: {:.1f}, TC: {:.2f}, P: {:.2f}mm, V: {:.2f}mm/s, dt: {:.5f}'.format(self.env_velocity, self.gain, self.tailchange, self.position, self.self_velocity, timedelta))
            self.since_last_print = 0
        self.since_last_print += 1
        #print('Grating: {:.1f}mm/s, Velocity: {:.2f}mm/s, Position: {:.2f}mm'.format(self.env_velocity, self.self_velocity, self.position))
        #print('Ev: {:.1f}, Bv: {:.3f}, A: {:.2f}, V: {:.3f}, P:{:.2f}, TCA:{:.5}'.format(self.env_velocity, self.self_velocity, self.acceleration, self.velocity, self.position, self.tailchange_accum))

    def describe(self):
        return {'Params': 
                {
                    "tau_accum": self.tau_accum, 
                    "tau_decay": self.tau_decay, 
                    "gain": self.gain,
                    "normalizer": self.normalizer
                    },
                'Velocity': float(self.velocity),
                'VelocitySelf': float(self.self_velocity),
                'VelocityEnv': float(self.env_velocity),
                'Acceleration': float(self.acceleration),
                'Position': float(self.position),
                'TailChange': float(self.tailchange),
                'TailChangeAccum': float(self.tailchange_accum),
                'TailPoints': self.tailpoints,
                'Mode': 'Closed'}

    def setVelocity(self, x):
        self.env_velocity = x
        return self

    def setAcceleration(self, acceleration):
        self.acceleration = acceleration
        return self

    def setPosition(self, position):
        self.position = position
        return self

    def processTailpoints(self, tailpoints):
        self.tailpoints = list(tailpoints)
        fp = np.array(tailpoints[0:4])
        lp = np.array(tailpoints[-3:])
        curve = lp.mean(axis=0) - fp.mean(axis=0)
        ratio = curve[0] / curve[1]
        tailangle = np.absolute(np.rad2deg(np.arctan(ratio)))
        
        tailchange = 6*np.tanh((max(abs(tailangle - self.last_tailangle) - 0.2, 0))/6)
        #tailchange = (max(abs(tailangle - self.last_tailangle) - 0.7, 0))
        self.last_tailangle = tailangle
        self.tailchange = tailchange