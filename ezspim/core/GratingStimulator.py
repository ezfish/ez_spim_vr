from psychopy import visual
from multiprocessing import Pipe
import multiprocessing
import psutil
import time


def monitor_slave(pipe, size, screen, downscale, screen_dist, orientation):
    p = psutil.Process()
    p.cpu_affinity([4,5])
    win_monitor = visual.Window([size[0]/downscale,size[1]/downscale],units="cm", color=(0, 0, 0), colorSpace='rgb',fullscr=False, screen=screen, monitor="proj")
    grating = visual.GratingStim(win=win_monitor, tex='sqr',mask= 'none',units='cm', pos=(0,0),size= (600/downscale,800/downscale),sf=0.81*5/screen_dist*downscale)
    grating.setOri(orientation)
    grating.setAutoDraw(True)
    while True:
        payload = pipe.recv()
        while pipe.poll():
            payload = pipe.recv()
        if payload[0] == 'd':
            grating.phase = payload[1]
            win_monitor.flip()
        elif payload[0] == 'h':
            return

def display_slave(pipe, size, screen_dist, orientation):
    p = psutil.Process()
    p.cpu_affinity([2,3])
    p.nice(psutil.HIGH_PRIORITY_CLASS)
    wnd = visual.Window(size,units="cm", color=(0, 0, 0), colorSpace='rgb',fullscr=True, screen=1, monitor="proj")
    grating = visual.GratingStim(win=wnd, tex='sqr',mask= 'none',units='cm', pos=(0,0),size= (600,800),sf=(0.81*18.75)/screen_dist) #sf used to be 0.81 (new profector: add factor 18.75 to yield period = 6mm, @ h = 3mm)
    grating.setOri(orientation)
    grating.setAutoDraw(True)
    starttime = time.perf_counter()
    framen = 0
    while True:
        payload = pipe.recv()
        while pipe.poll():
            payload = pipe.recv()
        if payload[0] == 'd':
            grating.phase = payload[1]
            wnd.flip()
            framen+=1
            if framen % 240 == 0:
                endtime = time.perf_counter()
                #print("GS: Compute time " + str((endtime-starttime)*1000) + "ms")
                starttime = endtime
        elif payload[0] == 'h':
            return


class GratingStimulator:
    def __init__(self, size, monitoring = False, screen_dist=3, screen_adjustment_factor=0.5, orientation=270):
        """
        Create the grating stimulator using supplied parameters.

        Args:
            size ([width, height]): Psychopy visual.Window size
            screen_dist (float): Distance of fish from screen in mm
            screen_adjustment_factor (float): Adjust the velocity of the grating to translate into real world units
            orientation (float): Orientation of the grating in degrees
        """
        self.adj_factor = screen_adjustment_factor
        self.screen_dist = screen_dist
        self.size = size
        self.monitoring = monitoring
        self.grating_phase = 0.0
        self.orientation = orientation
        
        self.display_parent_conn, self.display_child_conn = Pipe();
        self.proc_display = multiprocessing.Process(target=display_slave,
                                                    args=[
                                                        self.display_child_conn,
                                                        self.size,
                                                        screen_dist,
                                                        orientation
                                                        ]);
        self.proc_display.start()
        if self.monitoring:
            self.monitor_parent_conn, self.monitor_child_conn = Pipe();
            self.proc_monitor = multiprocessing.Process(target=monitor_slave,
                                                                 args=[
                                                                     self.monitor_child_conn,
                                                                     self.size,
                                                                     0,
                                                                     5,
                                                                     screen_dist,
                                                                     orientation
                                                                     ]);
            self.proc_monitor.start()
        self.draw(0.0);


    def draw(self,position):
        """
        Draws the grating using the supplied position

        Args:
            position (float): Position of the fish (Wiggle Model) in mm.
        """
        phase = position*(self.adj_factor)
        self.display_parent_conn.send(['d', phase]);
        self.grating_phase = phase
        if self.monitoring:
            self.monitor_parent_conn.send(['d', phase]);

    def describe(self): 
        return {'Phase': self.grating_phase, 'AdjFactor': self.adj_factor, 'ScreenDist': self.screen_dist, 'Ori': self.orientation };
    def halt(self):
        if self.monitoring:
            self.monitor_parent_conn.send(['h']);
        self.display_parent_conn.send(['h']);
