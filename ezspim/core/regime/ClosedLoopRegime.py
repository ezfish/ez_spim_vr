from ..wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
from ..CameraHandler import CameraHandler
from psychopy import event
import time

class ClosedLoopRegime:
    def __init__(self, data_logger, grating_stimulator, daq_manager, wiggle_model=ClosedLoopWiggleModel(), name="", duration=30.0, velocity=0.0,next_regime=None):
        self.velocity = velocity
        self.next = next_regime
        self.dur = duration
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.logger = data_logger
        self.name = name
        self.daq = daq_manager

    def setNext(self, next_regime):
        self.next = next_regime;
        pass
    
    def handleTailpoints(self, tailpoints):
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',(time.perf_counter() - self.logger.start_time))
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('Tailpoints',tailpoints)
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        self.wm.processTailpoints(tailpoints)
    
    def run(self):
        regime_start = time.perf_counter()
        regime_timestamp = time.perf_counter() - regime_start
        self.wm.setVelocity(self.velocity)
        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        saved_td=0.0

        CameraHandler.setCallback(self.handleTailpoints)
        while regime_timestamp < self.dur:
            oldts = regime_timestamp
            regime_timestamp = (time.perf_counter() - regime_start)
            tdelta = regime_timestamp - oldts
            saved_td+=tdelta

            self.wm.loop(tdelta)

            if saved_td > 0.005:
                self.logger.newDatapoint()
                self.logger.addValue('Name',self.name)
                self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
                self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
                self.logger.addValue('Fish',self.wm.describe())
                self.logger.addValue('TailCam',CameraHandler.describe())
                self.logger.addValue('GratingStimulator',self.gs.describe());
                saved_td = 0;

            self.gs.draw(self.wm.position)
            if len(event.getKeys())>0: return

        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        CameraHandler.setCallback(None)
        if self.next is not None:
            self.next.run();