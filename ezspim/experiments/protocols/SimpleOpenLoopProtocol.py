from ...core.regime.OpenLoopRegime import OpenLoopRegime
from ...core.wm.OpenLoopWiggleModel import OpenLoopWiggleModel
from ...core.CameraHandler import CameraHandler
from datetime import datetime
import time

class SimpleOpenLoopProtocol:
    def __init__(self, data_logger, grating_stimulator, daq_manager, velocity, wiggle_model=OpenLoopWiggleModel(), name="",  dur_stand = 30, dur_grat = 10):
        self.velocity = velocity
        self.dur_grat = dur_grat
        self.dur_stand = dur_stand
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.logger = data_logger
        self.name = name
        self.daq = daq_manager

       
        self.olr_grat = OpenLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                       self.name+"/Grating v={}".format(velocity), duration = dur_grat, velocity=velocity);
        self.olr_stand = OpenLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                        self.name+"/Standing stimulus", duration = dur_stand, velocity=0);

    def describe(self):
        return {
            "type": "SimpleOpenLoopProtocol",
            "name": self.name,
            "duration": {"standing": self.dur_stand, "grating": self.dur_grat},
            "velocity": self.velocity,
            "sequence": [
                {
                    "name": "OpenLoopRegime",
                    "duration": self.dur_grat,
                    "velocity": self.velocity,
                    "gain": 0
                    },
                {
                    "name": "OpenLoopRegime",
                    "duration": self.dur_stand,
                    "velocity": 0,
                    "gain": 0
                    }
                ]
            }
    def run(self):
        self.original_gain = self.wm.gain
        self.wm.gain = 1
        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());

        self.olr_grat.run()
        self.olr_stand.run()

        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/End')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        self.logger.flush()
        self.logger.close()
        self.wm.gain = self.original_gain