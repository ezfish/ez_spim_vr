from ...core.regime.ClosedLoopRegime import ClosedLoopRegime
from ...core.wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
from ...core.CameraHandler import CameraHandler
from datetime import datetime
import time

class FirstProtocol:
    def __init__(self, data_logger, grating_stimulator, daq_manager, velocity, gain, wiggle_model=ClosedLoopWiggleModel(), name="",  dur_closed = 120, dur_stand = 60):
        self.velocity = velocity
        self.gain = gain
        self.dur_closed = dur_closed
        self.dur_stand = dur_stand
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.logger = data_logger
        self.name = name
        self.daq = daq_manager

        self.clr_closed = ClosedLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                       self.name+"/ClosedV={}".format(velocity), duration = dur_closed, velocity=velocity);
        self.clr_stand = ClosedLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                        self.name+"/StandingStimulus", duration = dur_stand, velocity=0);

    def describe(self):
        return {
            "type": "FirstProtocol",
            "name": self.name,
            "duration": {"standing": self.dur_stand, "closed": self.dur_closed},
            "velocity": self.velocity,
            "gain": self.gain,
            "sequence": [
                {
                    "name": "ClosedLoopRegime",
                    "duration": self.dur_closed,
                    "velocity": self.velocity,
                    "gain": self.gain
                    },
                {
                    "name": "ClosedLoopRegime",
                    "duration": self.dur_stand,
                    "velocity": 0,
                    "gain": self.gain
                    }
                ]
            }

    def run(self):
        self.original_gain = self.wm.gain
        self.wm.gain = self.gain
        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        
        self.clr_closed.run()
        self.clr_stand.run()

        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/End')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        self.logger.flush()
        self.logger.close()
        self.wm.gain = self.original_gain
