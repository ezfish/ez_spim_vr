from ...core.regime.ClosedLoopRegime import ClosedLoopRegime
from ...core.wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
from ...core.CameraHandler import CameraHandler
from datetime import datetime
import time

class AdjustmentProtocol:
    def __init__(self, data_logger, grating_stimulator, daq_manager, velocity, gain, wiggle_model=ClosedLoopWiggleModel(), name="",  dur_closed = 120, dur_stand = 60):
        self.velocity = velocity
        self.gain = gain
        self.dur_closed = dur_closed
        self.dur_stand = dur_stand
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.logger = data_logger
        self.name = name
        self.daq = daq_manager

        self.start_pos = 0.0
        self.end_pos = 0.0
        self.start_normalizer = 0.0
        self.end_normalizer = 0.0

        self.clr_closed = ClosedLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                       self.name+"/ClosedV={}".format(velocity), duration = dur_closed, velocity=velocity);
        self.clr_stand = ClosedLoopRegime(self.logger, self.gs, self.daq, self.wm, 
                                        self.name+"/StandingStimulus", duration = dur_stand, velocity=0);

    def describe(self):
        return {
            "type": "AdjustmentProtocol",
            "name": self.name,
            "duration": {"standing": self.dur_stand, "closed": self.dur_closed},
            "velocity": self.velocity,
            "gain": self.gain,
            "sequence": [
                {
                    "name": "AdjustmentRegime",
                    "duration": self.dur_closed,
                    "velocity": self.velocity,
                    "start_normalizer": self.start_normalizer,
                    "end_normalizer": self.end_normalizer,
                    "start_pos": self.start_pos,
                    "end_pos": self.end_pos,
                    "gain": self.gain
                    },
                {
                    "name": "ClosedLoopRegime",
                    "duration": self.dur_stand,
                    "velocity": 0,
                    "gain": self.gain
                    }
                ]
            }

    def run(self):
        self.original_gain = self.wm.gain
        self.wm.gain = self.gain

        self.start_normalizer = self.wm.normalizer
        self.start_pos = self.wm.position
        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/Start')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());

        self.clr_closed.run()
        self.end_pos = self.wm.position
        pos_diff =  self.start_pos - self.end_pos
        drift = self.velocity * self.dur_closed
        fish_prop = pos_diff + drift
        factor = fish_prop / drift
        if factor < 0.5:
            factor = 0.5
        elif factor > 2:
            factor = 2
        factor = (factor +1) / 2
        self.wm.normalizer = self.wm.normalizer / factor
        self.end_normalizer = self.wm.normalizer

        self.clr_stand.run()
        print("New normalizer is {}.".format(self.wm.normalizer))
        print("Start_Pos is {}.".format(self.start_pos))
        print("End_Pos is {}.".format(self.end_pos))
        print("Drift is {}.".format(drift))
        print("Factor is {}.".format(factor))        
        self.logger.newDatapoint()
        self.logger.addValue('Name',self.name+'/End')
        self.logger.addValue('TimeStamp',time.perf_counter() - self.logger.start_time)
        self.logger.addValue('FrameCount',int(self.daq.getFrameCount()))
        self.logger.addValue('Fish',self.wm.describe())
        self.logger.addValue('TailCam',CameraHandler.describe())
        self.logger.addValue('GratingStimulator',self.gs.describe());
        self.logger.flush()
        self.logger.close()
        self.wm.gain = self.original_gain
        
