from ..core.DataLogger import DataLogger
from ..core.CameraHandler import CameraHandler
from .protocols.SimpleOpenLoopProtocol import SimpleOpenLoopProtocol
from datetime import datetime

class Severi14ControlExperiment:
    def __init__(self, data_folder, grating_stimulator, daq_manager, sample, velocities, wiggle_model, name="Severi14ControlExperiment", description="replicate head restained results from Figure 3 in severi et al 2014", tags = [], dur_stand = 30, dur_grat = 10, repeats=1):
        self.velocities = [float(i) for i in velocities]
        self.dur_grat = dur_grat
        self.dur_stand = dur_stand
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.descr = description
        self.name = name
        self.daq = daq_manager
        self.data_folder = data_folder
        self.repeats = repeats
        self.protocols = []
        self.tags = tags
        self.sample = sample

        prot_dicts = []
        date = datetime.now().isoformat().split('.')[0].replace(':','');
        for n in range(self.repeats):
            for i in range(len(self.velocities)):
                v = self.velocities[i]
                prot_log = data_folder + '\\{}_experiment{}_r{}_protocol{}.json'.format(date,name,n,i)
                logger = DataLogger(prot_log)
                self.protocols.append(SimpleOpenLoopProtocol(logger,self.gs,self.daq,v,self.wm,
                                                             self.name+"/SimpleOpenLoop={}".format(v),
                                                             dur_stand = dur_stand, dur_grat = dur_grat))
                prot_dicts.append({
                    "index": len(self.protocols)-1,
                    "protocol_id": str(logger.id),
                    "velocity": {"index": i, "value": v},
                    "log": prot_log,
                    "video": prot_log+".avi",
                    "protocol_meta": self.protocols[-1].describe(),
                    "repeat": n
                    })
        meta_dict = {
            "type": "SimpleOpenLoopExperiment",
            "name": name,
            "description": description,
            "sample": self.sample.describe(),
            "velocities": self.velocities,
            "duration": {"standing": dur_stand, "grating": dur_grat},
            "wiggle_model": wiggle_model.describe(),
            "protocols": prot_dicts,
            "tags": self.tags
            }
        prot_log = data_folder + '\\{}_experiment{}_meta.json'.format(date,name)
        logger = DataLogger(prot_log)
        logger.writeDatapoint(meta_dict)
        logger.flush()
        logger.close()
        self.id = logger.id

    def run(self):
        for p in self.protocols:
            CameraHandler.setOutputFile(p.logger.fname+".avi");
            p.run()
            p.logger.close()
