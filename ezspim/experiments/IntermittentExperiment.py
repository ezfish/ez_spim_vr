
from ..core.DataLogger import DataLogger
from ..core.CameraHandler import CameraHandler
from .protocols.FirstProtocol import FirstProtocol
from .protocols.IntermittentProtocol import IntermittentProtocol
from ..core.wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
from datetime import datetime

class IntermittentExperiment:
    def __init__(self, data_folder, grating_stimulator, daq_manager, sample, velocities, gains, 
                 wiggle_model=ClosedLoopWiggleModel(), name="IntermittentExperiment", description="Look for mismatch cells", tags = [], dur_intermittent = 10, dur_closed = 30, likelihood = 0.5):
        self.velocities = [float(i) for i in velocities]
        self.gains = [float(i) for i in gains]
        self.dur_intermittent = dur_intermittent
        self.dur_closed = dur_closed
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.name = name
        self.descr = description
        self.daq = daq_manager
        self.data_folder = data_folder
        self.protocols = []
        self.tags = tags
        self.sample = sample
        self.likelihood = likelihood

        prot_dicts = []
        date = datetime.now().isoformat().split('.')[0].replace(':','');
        
        for i in range(len(self.velocities)):
            v = self.velocities[i]
            for j in range(len(self.gains)):
                g = self.gains[j]

                prot_log = data_folder + '\\{}_experiment{}_protocol{}-{}.json'.format(date,name,i,j)
                logger = DataLogger(prot_log)
                self.protocols.append(IntermittentProtocol(logger,self.gs,self.daq,v,g,self.wm,
                                                    self.name+"/ClosedLoop={}-{}".format(v,g), dur_intermittent = dur_intermittent, dur_closed = dur_closed, likelihood = self.likelihood))
                prot_dicts.append({
                    "index": len(self.protocols)-1,
                    "protocol_id": str(logger.id),
                    "gain": {"index": j, "value": g},
                    "velocity": {"index": i, "value": v},
                    "log": prot_log,
                    "video": prot_log+".avi",
                    "protocol_meta": self.protocols[-1].describe()
                    })
        meta_dict = {
            "type": "IntermittentExperiment",
            "name": name,
            "description": description,
            "sample": self.sample.describe(),
            "velocities": self.velocities,
            "gains": self.gains,
            "duration": {"intermittent": dur_intermittent, "closed": dur_closed},
            "wiggle_model": wiggle_model.describe(),
            "protocols": prot_dicts,
            "tags": self.tags
            }
        prot_log = data_folder + '\\{}_experiment{}_meta.json'.format(date,name)
        logger = DataLogger(prot_log)
        logger.writeDatapoint(meta_dict)
        logger.flush()
        logger.close()
        self.id = logger.id

    def run(self):
        for p in self.protocols:
            CameraHandler.setOutputFile(p.logger.fname+".avi");
            p.run()

