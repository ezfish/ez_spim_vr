from ..core.DataLogger import DataLogger
from ..core.CameraHandler import CameraHandler
from .protocols.AdjustmentProtocol import AdjustmentProtocol
from ..core.wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
from datetime import datetime
from bson import objectid

class AdjustmentExperiment:
    def __init__(self, data_folder, grating_stimulator, daq_manager, sample, velocity, gain, 
                 wiggle_model=ClosedLoopWiggleModel(), name="Adjustment", description="", tags = [], dur_stand = 60, dur_closed = 120, stability_requirement=0.025):
        self.velocity = float(velocity)
        self.gain = float(gain)
        self.dur_closed = dur_closed
        self.dur_stand = dur_stand
        self.wm = wiggle_model
        self.gs = grating_stimulator
        self.name = name
        self.descr = description
        self.daq = daq_manager
        self.data_folder = data_folder
        self.protocols = []
        self.tags = tags
        self.stability_req = stability_requirement
        self.sample = sample
        
        self.date = datetime.now().isoformat().split('.')[0].replace(':','');
        self.id = objectid.ObjectId()

    def run(self):
        prot_dicts = []
        start_normalizer = self.wm.normalizer/10.0
        end_normalizer = self.wm.normalizer
        norm_memory = self.wm.normalizer/10.0

        while abs((end_normalizer-start_normalizer)/start_normalizer) > self.stability_req or abs((start_normalizer-norm_memory)/norm_memory) > self.stability_req:
            norm_memory = start_normalizer
            start_normalizer = end_normalizer
            v = self.velocity
            g = self.gain
            date = datetime.now().isoformat().split('.')[0].replace(':','');
            prot_log = self.data_folder + '\\{}_experiment{}_protocol{}.json'.format(date,self.name,start_normalizer)
            logger = DataLogger(prot_log)
            self.protocols.append(AdjustmentProtocol(logger,self.gs,self.daq,v,g,self.wm,
                                                self.name+"/SimpleOpenLoop={}-{}".format(v,g),
                                                dur_stand = self.dur_stand, dur_closed = self.dur_closed))

            p = self.protocols[-1]
            CameraHandler.setOutputFile(p.logger.fname+".avi");
            p.run()
            print("previous factor: {}".format(norm_memory / start_normalizer))
            end_normalizer = p.end_normalizer

            prot_dicts.append({
                "index": len(self.protocols)-1,
                "protocol_id": str(logger.id),
                "gain": { "value": g},
                "velocity": { "value": v},
                "normalizer": { 
                    "start": start_normalizer, 
                    "end": end_normalizer, 
                    "stability": (end_normalizer-start_normalizer)/start_normalizer
                    },
                "log": prot_log,
                "video": prot_log+".avi",
                "protocol_meta": self.protocols[-1].describe()
                })

        print("                                                                ")
        print("Automater Parameter Adjustment complete.")
        print("The NORMALIZER was found to be: {}".format(end_normalizer))
        print("                                                                ")


        meta_dict = {
            "type": "AdjustmentExperiment",
            "name": self.name,
            "description": self.descr,
            "sample": self.sample.describe(),
            "velocity": self.velocity,
            "gain": self.gain,
            "stability_req": self.stability_req,
            "duration": {"standing": self.dur_stand, "closed": self.dur_closed},
            "wiggle_model": self.wm.describe(),
            "protocols": prot_dicts,
            "tags": self.tags
            }
        prot_log = self.data_folder + '\\{}_experiment{}_meta.json'.format(self.date,self.name)
        logger = DataLogger(prot_log,id = self.id)
        logger.writeDatapoint(meta_dict)
        logger.flush()
        logger.close()