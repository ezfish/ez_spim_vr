if __name__ == '__main__':
    import pyximport; pyximport.install()
    from psychopy import visual
    from psychopy import event #import some libraries from PsychoPy
    import random
    from datetime import datetime, timedelta
    #from ezspim.core.wm.ClosedLoopWiggleModel import ClosedLoopWiggleModel
    #from ezspim.core.wm.SimpleClosedLoopWiggleModel import SimpleClosedLoopWiggleModel
    from ezspim.core.wm.AreaClosedLoopWiggleModel import AreaClosedLoopWiggleModel
    from ezspim.core.wm.AreaOpenLoopWiggleModel import AreaOpenLoopWiggleModel
    from ezspim.core.GratingStimulator import GratingStimulator
    from ezspim.core.CameraHandler import CameraHandler
    from ezspim.core.DataLogger import DataLogger
    from ezspim.core.DaqManager import DaqManager
    from ezspim.core.regime.OpenLoopRegime import OpenLoopRegime
    from ezspim.experiments.LandscapeExperiment import LandscapeExperiment
    from ezspim.experiments.SimpleOpenLoopExperiment import SimpleOpenLoopExperiment
    from ezspim.experiments.Severi14ControlExperiment import Severi14ControlExperiment
    from ezspim.experiments.Severi14ClosedLoopExperiment import Severi14ClosedLoopExperiment
    from ezspim.experiments.AdjustmentExperiment import AdjustmentExperiment
    from ezspim.core.Battery import Battery
    from ezspim.core.SampleDetails import SampleDetails
    import numpy as np
    from psychopy import core
    from random import shuffle
    import psutil
    import atexit
    import os
    import time
    from datetime import datetime
    import signal

def handleclick(event,x,y,flags,param):
    return

if __name__ == '__main__':
    p = psutil.Process()
    p.cpu_affinity([6,7])
    p.nice(psutil.REALTIME_PRIORITY_CLASS)
    
    wait_for_clock_signal =  False

    gs = GratingStimulator([1280,720], True)

    wmC = AreaClosedLoopWiggleModel(tau_accum=0.025, tau_decay=0.01, normalizer =21000, track_bouts=False) #~4000 - 20000 according to Daniel's free swimimng model.
    wmO = AreaOpenLoopWiggleModel(tau_accum=0.025, tau_decay=0.01, normalizer =21000) #~4000 - 20000 according to Daniel's free swimimng model.

    #wm = AreaClosedLoopWiggleModel(tau_accum=0.008, tau_decay=0.008, normalizer =100000) #~4000 - 20000 according to Daniel's free swimimng model.
    #wm = SimpleClosedLoopWiggleModel(tau_accum=0.025, tau_decay=0.001, normalizer =10000) #~4000 - 20000 according to Daniel's free swimimng model.
    daq = DaqManager()


    #########################################################
    # SETTINGS
    global_tags = ["Test"]
   
    #Sample Details
    Sample_ID = 142 #Sample ID.
    Age = 6 #Age if fish in dpf.
    Genotype = ["HuC:H2B-GCaMP6f","Nacre","1286ox"]
    Description = "pre-screened, repeat Severietal14"
    sd = SampleDetails(Sample_ID, Age, Genotype, Description)
    #########################################################

    #Prepare directories
    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    base_dir = 'C:\\experiments\\'
    DataLogger.base_dir = base_dir;
    print("Saving to: {}".format(base_dir))

    #Battery
    battery_dir = dt+'_battery\\'
    os.mkdir(base_dir+battery_dir)
    battery = Battery(battery_dir)
    
    #velocities = np.asarray([3, 5, 8, 10, 15, 20, 25, 30, 40],np.float32)/5
    velocities = np.asarray([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],np.float32)/5
    #velocities = np.asarray([40],np.float32)/5
    #velocities = np.repeat(velocities, 5)
    #np.random.shuffle(velocities)
    #gains = [0.25, 0.25, 0.25, 0.25, 0.25, 0.5, 0.5, 0.5, 0.5, 0.5, 0.75, 0.75, 0.75, 0.75, 0.75, 1, 1, 1, 1, 1, 1.25, 1.25, 1.25, 1.25, 1.25, 1.5, 1.5, 1.5, 1.5, 1.5, 1.75, 1.75, 1.75, 1.75, 1.75]
    gains = [1]
    
    #experiment - adjustment
    experiment_name = "adjustmentexperiment_0"
    description = "find normaliser to set gain = 1"
    tags = global_tags + []
    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    experiment_dir = dt+"_"+experiment_name+"\\"
    os.mkdir(base_dir+battery_dir+experiment_dir)
    experiment = AdjustmentExperiment(battery_dir+experiment_dir,gs,daq,sd,10.0/5.0,1.0,wmC,name=experiment_name, description=description, tags=tags, dur_stand =10, dur_closed = 10, stability_requirement=0.05)
    battery.append(experiment)

    #wmO.normalizer = wmC.normalizer

    #Experiment 
    #experiment_name = "Severi14ControlExp_1"
    #description = "replicate head restained results from Figure 3 in severi et al 2014"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + [""]
    ##experiment = Severi14ControlExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,wmO,name=experiment_name, description=description, tags=tags, dur_stand =30, dur_grat = 10, repeats=1)
    #experiment = Severi14ControlExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,wmO,name=experiment_name, description=description, tags=tags, dur_stand =1, dur_grat = 5, repeats=5)
    #battery.append(experiment)

    ##experiment - adjustment
    #experiment_name = "adjustmentexperiment"
    #description = "find normaliser to set gain = 1"
    #tags = global_tags + []
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #experiment = AdjustmentExperiment(battery_dir+experiment_dir,gs,daq,sd,10.0/5.0,1.0,wmC,name=experiment_name, description=description, tags=tags, dur_stand =10, dur_closed = 10, stability_requirement=0.05)
    #battery.append(experiment)

    #Experiment 
    experiment_name = "Severi14ClosedLoopExp_1"
    description = "replicate head restained results from Figure 3 in severi et al 2014 in CLOSED LOOP"
    dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    experiment_dir = dt+"_"+experiment_name+"\\"
    os.mkdir(base_dir+battery_dir+experiment_dir)
    tags = global_tags + [""]
    experiment = Severi14ClosedLoopExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wmC,name=experiment_name, description=description, tags=tags, dur_stand =30, dur_closed = 10)
    battery.append(experiment)

    ##Experiment 
    #experiment_name = "Simple_Gain_Experiment"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain= 0.5, 1, 1.5, shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + ["laserTest"]
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)

    ##Experiment 1
    #experiment_name = "Landscape_Experiment_1"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain=0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + []
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)
    
    ##Experiment 2
    #experiment_name = "Landscape_Experiment_2"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain=0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75 shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + []
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)
   
    ##Experiment 3
    #experiment_name = "Landscape_Experiment_3"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain=0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + []
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)

    ##Experiment 4
    #experiment_name = "Landscape_Experiment_4"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain=0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + []
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)

    ##Experiment 5
    #experiment_name = "Landscape_Experiment_5"
    #description = "Step through env_vels from 2 to 20, 5 * 10sec at gain=0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, shuffled speeds"
    #dt = datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')
    #experiment_dir = dt+"_"+experiment_name+"\\"
    #os.mkdir(base_dir+battery_dir+experiment_dir)
    #tags = global_tags + []
    #experiment = LandscapeExperiment(battery_dir+experiment_dir,gs,daq,sd,velocities,gains,wm,name=experiment_name, description=description, tags=tags, dur_stand =20, dur_closed = 10)
    #battery.append(experiment)

    CameraHandler.init(fit_tail=True,showcam=False);
    time.sleep(3)
    CameraHandler.start()
    time.sleep(3)
    if wait_for_clock_signal: daq.block()

    
    def halt():
        print("HALTING")
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()
    atexit.register(halt)
    signal.signal(signal.SIGTERM, halt)
    signal.signal(signal.SIGINT, halt)
    #signal.signal(signal.CTRL_BREAK_EVENT, halt)

    try:
        battery.run()
        print("DONEDONEDONEDONEDONEDONEDONEDONEDONEDONE")
    finally:
        CameraHandler.halt()
        #mywin.close()
        daq.close()
        gs.halt()
    print("DONE")
        #if __name__ == '__main__':
        #    core.quit()
